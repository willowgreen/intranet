<pre>
<?php
$json = json_decode(file_get_contents('navbar.json'), true);

foreach ($json as $category => $contents) {
    ?>
    <li class="no-padding">
    <ul class="collapsible collapsible-accordion light-blue darken-1">
    <li class="bold"><a
    class="collapsible-header waves-effect white-text"><?= $category; ?></a>
    <div class="collapsible-body light-blue accent-2">
    <ul>
    <?php 
    foreach ($contents as $item => $attributes) {
        echo ($item);
    ?>
    <li><a class="waves-effect white-text" style="font-weight: bold;"
        href="<?= $attributes['link']; ?>"><?= $item; ?></a></li>
            <?php
    }
    ?>
            </ul>
            </div></li>
            </ul>
            </li>
            <?php
}
?>
</pre>