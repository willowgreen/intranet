<?php
require_once (__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'config.php');
if (isset($_POST['submit'])) {
    $configuration = array();
    foreach (Config::getAllProperties() as $property) {
        $configuration[$property] = $_POST[$property];
    }
    
    foreach ($configuration as $key => $value) {
        $handle = fopen(Config::getConfigDir() . DIRECTORY_SEPARATOR . $key, 'w');
        fwrite($handle, $value);
        fclose($handle);
    }
    
    @header('Location: ./');
}

require_once (Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'style' . DIRECTORY_SEPARATOR . 'header.php');
if (in_array($_SESSION['user']->getClass(), Intranet::$GROUP_ADMINISTRATORS)) {
    ?>
<body class="light-blue darken-4">
	<main class="light-blue darken-3"> <br>
	<div class="container">
		<div class="card-panel">
			<h4 align="center">Manage System</h4>
			<p>Here you can configure the intranet to your liking. If there is a
				number (0 or 1) where it doesn't make sense, this number is 0 for
				false, or 1 for true, in answer to its property. If you don't
				understand a setting, please don't touch it! It could break
				everything.</p>
			<form action="./" method="POST">
				<table>
			
		<?php
    foreach (Config::getAllPropertiesWithValues() as $key => $value) {
        ?>
        <tr>
						<th>
							<p style="text-align: left; margin-top: 0px; margin-bottom: 0px;">
								<b><?= $key ?></b>
							</p>
						</th>

						<td width="80%"><input style="width: 100%;" type="text"
							class="browser-default" id="<?= $key ?>" name="<?= $key ?>"
							type="text" value="<?= $value ?>"></td>
					</tr>
	<?php
    }
    ?>
		</table>
				<br>
				<div align="center">
					<button class="btn waves-effect waves-light cyan lighten-2"
						type="submit" name="submit">Save changes</button>
					&nbsp; <a class="btn waves-effect waves-light light-blue darken-3"
						href="<?= isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : Config::getProperty('BaseDirWebserver') ?>">Back</a>

				</div>
				<br>
			</form>
		</div>
	</div>
	<br>
	</main>
	<?php
    require_once (Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'style' . DIRECTORY_SEPARATOR . 'footer.php');
    if (! in_array($_SESSION['user']->getClass(), Intranet::$GROUP_ADMINISTRATORS)) {
        header('Location: ' . Config::getProperty('BaseDirWebserver'));
    }
}
