<?php
require_once (__DIR__ . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'Mobile_Detect' . DIRECTORY_SEPARATOR . 'Mobile_Detect.php');
require_once (__DIR__ . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'phpqrcode' . DIRECTORY_SEPARATOR . 'phpqrcode.php');
require_once (__DIR__ . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'PHPMailer' . DIRECTORY_SEPARATOR . 'PHPMailerAutoload.php');
require_once (__DIR__ . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'nschlobohm' . DIRECTORY_SEPARATOR . 'Document.php');
require_once (__DIR__ . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'nschlobohm' . DIRECTORY_SEPARATOR . 'User.php');
require_once (__DIR__ . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'nschlobohm' . DIRECTORY_SEPARATOR . 'TrainingLicence.php');
require_once (__DIR__ . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'nschlobohm' . DIRECTORY_SEPARATOR . 'InductionDocument.php');
require_once (__DIR__ . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'nschlobohm' . DIRECTORY_SEPARATOR . 'InductionResult.php');
require_once (__DIR__ . DIRECTORY_SEPARATOR . 'lib' . DIRECTORY_SEPARATOR . 'nschlobohm' . DIRECTORY_SEPARATOR . 'IntranetMail.php');

if (session_status() === PHP_SESSION_NONE) {
    session_start();
}

if (! isset($_SESSION['WELCOME_TOAST']))
    $_SESSION['WELCOME_TOAST'] = false;

//
class Config
{

    public function getConfigDir()
    {
        return __DIR__ . DIRECTORY_SEPARATOR . '-config';
    }

    public function getAllProperties()
    {
        return array_diff(scandir(Config::getConfigDir()), array(
            '.',
            '..',
            '.htaccess'
        ));
    }

    public function getAllPropertiesWithValues()
    {
        $propertiesWithValues = array();
        
        foreach (Config::getAllProperties() as $key) {
            $propertiesWithValues[$key] = file_get_contents(Config::getConfigDir() . DIRECTORY_SEPARATOR . $key);
        }
        
        return $propertiesWithValues;
    }

    public function getProperty($property)
    {
        $value = file_get_contents(Config::getConfigDir() . DIRECTORY_SEPARATOR . $property);
        $value = str_replace('__DIR__', __DIR__, $value);
        
        return $value;
    }
}

//
class Intranet // DON'T TOUCH ANYTHING IN HERE UNLESS YOU KNOW EXACTLY WHAT YOU'RE DOING
{

    public static function getVersion()
    {
        return @file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'VERSION');
    }

    public static $GROUP_ADMINISTRATORS = array(
        // developers
        'developer',
        
        // rest
        'administrator'
    );

    public function getLogo_base64()
    {
        return base64_encode(fread(fopen(Config::getProperty('SiteLogo'), 'r'), filesize(Config::getProperty('SiteLogo'))));
    }

    public function getLogoWhite_base64()
    {
        return base64_encode(fread(fopen(Config::getProperty('SiteLogoWhite'), 'r'), filesize(Config::getProperty('SiteLogoWhite'))));
    }

    public static $GROUP_CONTRIBUTORS = array(
        // developers
        'developer',
        
        // administrators
        'administrator',
        
        // rest
        'employee'
    );

    public static $GROUP_INDUCTEES = array(
        // developers
        'developer',
        
        // administrators
        'administrator',
        
        // employees
        'employee',
        
        // rest
        'contractor'
    );

    public function getDbLink()
    {
        $link = @new mysqli(Config::getProperty('DatabaseHost') . ':' . Config::getProperty('DatabasePort'), Config::getProperty('DatabaseUser'), Config::getProperty('DatabasePassword'), Config::getProperty('DatabaseDbName'));
        if (! $link->connect_error)
            return $link;
        else
            die('ERROR: Not possible to connect to ' . Config::getProperty('DatabaseHost') . ':' . Config::getProperty('DatabasePort') . '/' . Config::getProperty('DatabaseDbName') . ' as ' . Config::getProperty('DatabaseUser') . '. Contact the system administrator if this problem persists.');
    }

    public function getDbNumQueries()
    {
        $result = Intranet::getDbLink()->query('SHOW SESSION STATUS LIKE \'Queries\';');
        return $result->fetch_all()[0][1];
    }

    public function getVersionHash()
    {
        return md5(Intranet::getVersion());
    }

    public function getLatestRevision()
    {
        return @file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . '.git' . DIRECTORY_SEPARATOR . 'refs' . DIRECTORY_SEPARATOR . 'remotes' . DIRECTORY_SEPARATOR . 'origin' . DIRECTORY_SEPARATOR . 'master');
    }

    public function getNavbar()
    {
        return @json_decode(@file_get_contents(Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'navbar.json'), true);
    }
}

//
function getFolderSize($folder)
{
    $size = 0;
    $folder = realpath($folder);
    if ($folder !== false) {
        // then it's actually something that exists (and doesn't eval to false)
        foreach (new RecursiveIteratorIterator(new RecursiveDirectoryIterator($folder, FilesystemIterator::SKIP_DOTS)) as $rii) {
            $size += $rii->getSize();
        }
    }
    return $size;
}

//
function GUID()
{
    if (function_exists('com_create_guid') === true) {
        return strtolower(trim(com_create_guid(), '{}'));
    }
    
    return strtolower(sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535)));
}

date_default_timezone_set(Config::getProperty('Timezone'));
// a PHP-compatible timezone http://php.net/manual/en/timezones.php

// if ((! isset($_SESSION['login']) || ! $_SESSION['login']) && ! defined('LOGIN') && ! defined('INDUCTION_VIEW')) {
// http://localhost/intranet/Documents/Manage/
if (! isset($_SESSION['login']) || ! $_SESSION['login'] || ! isset($_SESSION['user']) || ! $_SESSION['user']) {
    if (! defined('LOGIN') && ! defined('INDUCTION_VIEW')) {
        if (! defined('INDEX'))
            $_SESSION['PRELOGIN_REQ'] = $_SERVER['REQUEST_URI'];
        
        header('Location: ' . Config::getProperty('BaseDirWebserver') . '/Users/Auth/Login');
    }
}

$NUMQUERIES_BEGINNING = Intranet::getDbNumQueries();
