<?php
require_once (__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'config.php');
$_SESSION['login'] = false;
@setcookie('WELCOME_TOAST', FALSE, time() - 1);
unset($_COOKIE['WELCOME_TOAST']);
session_destroy();
@header('Location: ' . Config::getProperty('BaseDirWebserver'));
