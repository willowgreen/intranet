<?php
define('LOGIN', true);
require_once (__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'config.php');

if (isset($_SESSION['login']) && $_SESSION['login']) {
    if (isset($_SESSION['PRELOGIN_REQ'])) {
        @header('Location: ' . $_SESSION['PRELOGIN_REQ']);
        $_SESSION['PRELOGIN_REQ'] = Config::getProperty('BaseDirWebserver');
        unset($_SESSION['PRELOGIN_REQ']);
    }
    header('Location: ' . Config::getProperty('BaseDirWebserver'));
}

if (isset($_POST['submit'])) {
    if (isset($_POST['email']) && isset($_POST['password'])) {
        $user = Intranet::getDbLink()->query('SELECT DISTINCT `password`, `id`, `suspended` FROM `users` WHERE `email`=\'' . Intranet::getDbLink()->real_escape_string($_POST['email']) . '\';');
        $user = $user->fetch_all(MYSQLI_ASSOC)[0];
        if (password_verify($_POST['password'], $user['password']) && ! (bool) $user['suspended']) {
            $_SESSION['login'] = true;
            $_SESSION['user'] = User::fromDatabase($user['id']);
            if (isset($_SESSION['PRELOGIN_REQ'])) {
                @header('Location: ' . $_SESSION['PRELOGIN_REQ']);
                $_SESSION['PRELOGIN_REQ'] = Config::getProperty('BaseDirWebserver');
                unset($_SESSION['PRELOGIN_REQ']);
            } else {
                header('Location: ' . Config::getProperty('BaseDirWebserver'));
            }
        } else {
            header('Location: ./?error');
        }
    } else {
        header('Location: ./?error');
    }
}

require_once (Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'style' . DIRECTORY_SEPARATOR . 'header.php');
?>
<body class="light-blue darken-2">
	<br>
	<br>
	<div class="container">
	<?= Config::getProperty('SiteUseLogo') ? '<br><img style="float: right; max-width: 290px;" src="data:image/png;base64,' . Intranet::getLogoWhite_base64() . '">' : '<h4 align="center">' . Config::getProperty('SiteTitle') . '</h4>'?>
	<br> <br>
		<h2 class="white-text" style="font-weight: 100;">Login</h2>
		<br>	
		<?php
if (isset($_GET['error'])) {
    ?>
		<p style="font-size: 18px;" class="bold white-text">
			Error logging in. Check that you have entered your email and password
			correctly! <b>Your account may be suspended</b>. Check with your
			system administrator.
		</p>
		<br>
				<?php
}
?>
		<form action="./" method="POST">
			<div class="input-field">
				<input id="email" name="email" type="text" class="white-text"> <label
					for="email">Username/Email</label>
			</div>
			<div class="input-field">
				<input id="password" name="password" type="password"
					class="white-text"> <label for="password">Password</label>
			</div>
			<button class="btn waves-effect waves-light white black-text"
				type="submit" name="submit">Submit</button>
		</form>
	</div>
</body>
<script type="text/javascript">
$(function() {
  $('#email').focus().select();
});
</script>
<?php
require_once (Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'style' . DIRECTORY_SEPARATOR . 'footer.php');
