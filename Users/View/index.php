<?php
require_once (__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'config.php');
$id = $_GET['id'];
$user = User::fromDatabase($id);

require_once (Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'style' . DIRECTORY_SEPARATOR . 'header.php');
?>
<body class="light-blue darken-4">
	<main class="light-blue darken-3"> <br>
	<div class="container">
		<div class="card-panel">
			<table>
				<tr>
					<td><img src="<?= $user->getPictureImgContent(); ?>"
						alt="<?= $user->getNameFull(); ?>'s picture" class="circle"
						style="height: 100px; width: 100px; object-fit: cover;"></td>
					<td><h4><?= $user->getNameFull() ?> (<?= ucfirst( $user->getClass () ) ?>)</h4></td>
				</tr>
			</table>
			<table>
				<tbody>
					<tr style="line-height: 1px;">
						<th width="50%">Email Address</th>
						<td><a href="mailto:<?= $user->getEmail(); ?>"
							title="Email <?= $user->getNameFull(); ?>"><?= $user->getEmail(); ?></a></td>
					</tr>
					<tr style="line-height: 1px;">
						<th>Name</th>
						<td><?= $user->getNameFull(); ?></td>
					</tr>
					<tr style="line-height: 1px;">
						<th>Emergency Contact Name</th>
						<td><?= $user->getEmergencyContact_Name() != '' ? $user->getEmergencyContact_Name() : '<small>(field empty)</small>'; ?></td>
					</tr>
					<tr style="line-height: 1px;">
						<th>Emergency Contact Relationship</th>
						<td><?= $user->getEmergencyContact_Relationship() != '' ? $user->getEmergencyContact_Relationship() : '<small>(field empty)</small>'; ?></td>
					</tr>
					<tr style="line-height: 1px;">
						<th>Emergency Contact Number</th>
						<td><?= $user->getEmergencyContact_Number() != '' ? $user->getEmergencyContact_Number() : '<small>(field empty)</small>'; ?></td>
					</tr>
					<tr style="line-height: 1px;">
						<th>Suspended?</th>
						<td><?= $user->getSuspended() ? 'Yes' : 'No'; ?></td>
					</tr>
					<tr style="line-height: 1px;">
						<th>ID</th>
						<td><code><?= $user->getId(); ?></code></td>
					</tr>

				</tbody>
			</table>
			<br>
			<p align="center">
				<a class="waves-effect waves-light btn light-blue darken-3"
					href="./../Manage/?edit&id=<?= $user->getId(); ?>"
					title="Edit this user">Edit</a> &nbsp; <a
					class="waves-effect waves-light btn <?= $user->getSuspended() ? 'green accent-4' : 'amber darken-3'; ?>"
					href="./../Manage/?suspend&id=<?= $user->getId(); ?>"
					title="<?= $user->getSuspended() ? 'Uns' : 'S' ?>uspend this user"><?= $user->getSuspended() ? 'Uns' : 'S'; ?>uspend</a>
				&nbsp; <a class="waves-effect waves-light btn red darken-3"
					href="./../Manage/?delete&id=<?= $user->getId(); ?>"
					title="Delete this user">Delete</a>
			</p>
			<p align="center">
				<a class="waves-effect waves-light btn light-blue darken-3"
					href="<?= (isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : './../Manage'; ?>"
					title="Go back">Back</a>
			</p>
		</div>
	</div>
	</main>
<?php
require_once (Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'style' . DIRECTORY_SEPARATOR . 'footer.php');
