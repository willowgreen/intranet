<?php
require_once (__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'config.php');
header('Location: ' . Config::getProperty('BaseDirWebserver') . '/Users/View/?id=' . $_SESSION['user']->getId());
?>
<html>
<title>Redirect</title>
<meta http-equiv="refresh"
	content="0; ./../View/?id=<?= $_SESSION['user']->getId(); ?>">
</html>