<?php
require_once (__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'config.php');
if (in_array($_SESSION['user']->getClass(), Intranet::$GROUP_ADMINISTRATORS)) {
    require_once (Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'style' . DIRECTORY_SEPARATOR . 'header.php');
    ?>
<body class="light-blue darken-4">
	<main class="light-blue darken-3"> <br>
	<div class="container">
		<div class="card-panel">
			<h4 align="center">Manage Users</h4>
		<?php
    if (isset($_GET['create'])) {
        if (isset($_POST['submit'])) {
            $id = GUID();
            $class = Intranet::getDbLink()->real_escape_string($_POST['user_class']);
            $email = Intranet::getDbLink()->real_escape_string($_POST['email']);
            $password = Intranet::getDbLink()->real_escape_string($_POST['password']);
            $password = password_hash($password, PASSWORD_BCRYPT);
            $name_first = Intranet::getDbLink()->real_escape_string($_POST['name_first']);
            $name_last = Intranet::getDbLink()->real_escape_string($_POST['name_last']);
            $emergency_name = Intranet::getDbLink()->real_escape_string($_POST['emergency_name']);
            $emergency_relationship = Intranet::getDbLink()->real_escape_string($_POST['emergency_relationship']);
            $emergency_number = Intranet::getDbLink()->real_escape_string($_POST['emergency_number']);
            Intranet::getDbLink()->query('INSERT INTO `users` (`id`, `class`, `email`, `password`, `name_first`, `name_last`, `emergency_name`, `emergency_relationship`, `emergency_number`, `suspended`) VALUES (\'' . $id . '\', \'' . $class . '\', \'' . $email . '\', \'' . $password . '\', \'' . $name_first . '\', \'' . $name_last . '\', \'' . $emergency_name . '\', \'' . $emergency_relationship . '\', \'' . $emergency_number . '\', 0);');
            // IntranetMail::sendWelcome ( $id );
            header('Location: ./../View/?id=' . $id);
        }
        ?>
		<h5>Create User</h5>
			<form action="<?= basename(__FILE__) ?>?create" method="POST">
				<div class="input-field">
					<select id="user_class" name="user_class">
						<option value="contractor" disabled selected>User class</option>
						<option value="contractor">Contractor</option>
						<option value="employee">Employee</option>
						<option value="administrator">Administrator</option>
					</select>
				</div>
				<div class="input-field">
					<input id="email" name="email" type="text" class="validate"> <label
						class="active" for="email">Email Address</label>
				</div>
				<div class="input-field">
					<input id="name_first" name="name_first" type="text"
						class="validate"> <label class="active" for="name_first">First
						Name</label>
				</div>
				<div class="input-field">
					<input id="name_last" name="name_last" type="text" class="validate">
					<label class="active" for="name_last">Last Name </label>
				</div>
				<div class="input-field">
					<input id="emergency_name" name="emergency_name" type="text"
						class="validate"> <label class="active" for="emergency_name">Emergency
						Contact Name</label>
				</div>
				<div class="input-field">
					<input id="emergency_relationship" name="emergency_relationship"
						type="text" class="validate"> <label class="active"
						for="emergency_relationship">Emergency Contact Relationship</label>
				</div>
				<div class="input-field">
					<input id="emergency_number" name="emergency_number" type="text"
						class="validate"> <label class="active" for="emergency_number">Emergency
						Contact Number</label>
				</div>
				<div class="input-field">
					<input id="password" name="password" type="password"
						class="validate"> <label class="active" for="password">Password</label>
				</div>
				<br> <br>
				<button class="btn waves-effect waves-light light-blue darken-3"
					type="submit" name="submit">Submit</button>
				<br>
			</form>
			<script>
			$(document).ready(function() {
			    $('select').material_select();
			});
			</script>
		<?php
    } else if (isset($_GET['delete'])) {
        if (! isset($_GET['id'])) {
            header('Location: ' . basename(__FILE__));
        }
        $id = $_GET['id'];
        
        if (isset($_GET['confirm'])) {
            Intranet::getDbLink()->query('DELETE FROM `users` WHERE `id`=\'' . $id . '\' AND `class`!=\'dev\' AND `class`!=\'administrator\';');
            Intranet::getDbLink()->query('DELETE FROM `training_licences` WHERE `recipient`=\'' . $id . '\';');
            Intranet::getDbLink()->query('DELETE FROM `inductions` WHERE `recipient`=\'' . $id . '\';');
            @header('Location: ./');
            echo ('<meta http-equiv="refresh" content="0; url=./">');
            die();
        }
        $user = User::fromDatabase($id);
        ?>
		<h5>Delete User</h5>
			<br> <br>
			<!-- delet this -->
			<p align="center">
				<b>Are you sure you want to delete this user?</b> <br>
			</p>

			<table>
				<tbody>
					<tr>
						<th>Email</th>
						<td><?= $user->getEmail(); ?></td>
					</tr>
					<tr>
						<th>Name</th>
						<td><?= $user->getNameFull(); ?></td>
					</tr>
				</tbody>
			</table>
			<br>
			<p align="center">
				<a class="waves-effect waves-light btn green accent-4"
					href="./../View/?id=<?= $user->getId(); ?>">No</a> &nbsp; <a
					class="waves-effect waves-light btn amber darken-3"
					href="<?= $_SERVER['REQUEST_URI'] ?>&confirm">Yes</a>
			</p>
		<?php
    } else if (isset($_GET['edit'])) {
        if (! isset($_GET['id'])) {
            @header('Location: ./');
            echo ('<meta http-equiv="refresh" content="0; url=./">');
            die();
        }
        $id = $_GET['id'];
        $user = User::fromDatabase($id);
        if (isset($_POST['submit'])) {
            $class = Intranet::getDbLink()->real_escape_string($_POST['user_class']);
            $email = Intranet::getDbLink()->real_escape_string($_POST['email']);
            $password = Intranet::getDbLink()->real_escape_string($_POST['password']);
            $emergency_name = Intranet::getDbLink()->real_escape_string($_POST['emergency_name']);
            $emergency_relationship = Intranet::getDbLink()->real_escape_string($_POST['emergency_relationship']);
            $emergency_number = Intranet::getDbLink()->real_escape_string($_POST['emergency_number']);
            
            if ($user->getClass() != $class) {
                Intranet::getDbLink()->query('UPDATE `users` SET `class`=\'' . $class . '\' WHERE `id`=\'' . $user->getId() . '\';');
            }
            
            if ($user->getEmail() != $email) {
                Intranet::getDbLink()->query('UPDATE `users` SET `email`=\'' . $email . '\' WHERE `id`=\'' . $user->getId() . '\';');
            }
            
            if ($user->getEmergencyContact_Name() != $emergency_name) {
                Intranet::getDbLink()->query('UPDATE `users` SET `emergency_name`=\'' . $emergency_name . '\' WHERE `id`=\'' . $user->getId() . '\';');
            }
            
            if ($user->getEmergencyContact_Relationship() != $emergency_relationship) {
                Intranet::getDbLink()->query('UPDATE `users` SET `emergency_relationship`=\'' . $emergency_relationship . '\' WHERE `id`=\'' . $user->getId() . '\';');
            }
            
            if ($user->getEmergencyContact_Number() != $emergency_number) {
                Intranet::getDbLink()->query('UPDATE `users` SET `emergency_number`=\'' . $emergency_number . '\' WHERE `id`=\'' . $user->getId() . '\';');
            }
            
            if ($password != '') {
                Intranet::getDbLink()->query('UPDATE `users` SET `password`=\'' . password_hash($password, PASSWORD_BCRYPT) . '\' WHERE `id`=\'' . $user->getId() . '\';');
            }
            
            @header('Location: ./../View/?id=' . $user->getId());
            echo ('<meta http-equiv="refresh" content="0; url=./../View?id=' . $user->getId() . '">');
            die();
        }
        ?>
		<h5>Edit User</h5>
			<form action="<?= basename($_SERVER['REQUEST_URI']) ?>" method="POST">
				<div class="input-field">
					<select id="user_class" name="user_class">
						<option value="<?= $user->getClass(); ?>">Choose user class</option>
						<option value="contractor"
							<?= $user->getClass()=='contractor' ? 'selected' : ''; ?>>Contractor</option>
						<option value="employee"
							<?= $user->getClass()=='employee' ? 'selected' : ''; ?>>Employee</option>
						<option value="administrator"
							<?= $user->getClass()=='administrator' ? 'selected' : ''; ?>>Administrator</option>
					</select>
				</div>
				<br>
				<div class="input-field">
					<input id="email" name="email" type="text" class="validate"
						value="<?= $user->getEmail(); ?>"> <label class="active"
						for="email">Email Address</label>
				</div>
				<div class="input-field">
					<input id="password" name="password" type="password"
						class="validate"> <label class="active" for="password">Password</label>
				</div>
				<div class="input-field">
					<input id="emergency_name" name="emergency_name" type="text"
						class="validate" value="<?= $user->getEmergencyContact_Name(); ?>">
					<label class="active" for="emergency_name">Emergency Contact Name</label>
				</div>
				<div class="input-field">
					<input id="emergency_relationship" name="emergency_relationship"
						type="text" class="validate"
						value="<?= $user->getEmergencyContact_Relationship(); ?>"> <label
						class="active" for="emergency_relationship">Emergency Contact
						Relationship</label>
				</div>
				<div class="input-field">
					<input id="emergency_number" name="emergency_number" type="text"
						class="validate"
						value="<?= $user->getEmergencyContact_Number(); ?>"> <label
						class="active" for="emergency_number">Emergency Contact Number</label>
				</div>
				<br> <br>
				<div class="center">
					<button
						class="btn waves-effect waves-light light-blue cyan lighten-2"
						type="submit" name="submit">Submit</button>
					&nbsp; <a class="btn waves-effect waves-light light-blue darken-3"
						href="./../View/?id=<?= $user->getId(); ?>">Back</a>
				</div>
				<br>
			</form>
			<script>
			$(document).ready(function() {
			    $('select').material_select();
			});
			</script>
		<?php
    } else if (isset($_GET['suspend'])) {
        if (! isset($_GET['id'])) {
            @header('Location: ./');
            echo ('<meta http-equiv="refresh" content="0; url=./">');
            die();
        }
        $id = $_GET['id'];
        
        $user = User::fromDatabase($id);
        
        if (isset($_GET['confirm'])) {
            if ($user->getSuspended())
                Intranet::getDbLink()->query('UPDATE `users` SET `suspended`=0 WHERE `id`=\'' . $user->getId() . '\';');
            else
                Intranet::getDbLink()->query('UPDATE `users` SET `suspended`=1 WHERE `id`=\'' . $user->getId() . '\' AND `class`!=\'dev\';');
            @header('Location: ./../View/?id=' . $user->getId());
            echo ('<meta http-equiv="refresh" content="0; url=./../View/?id=' . $user->getId() . '">');
            die();
        }
        ?>
		<h5><?= $user->getSuspended() ? 'Uns' : 'S' ?>uspend User</h5>
			<br> <br>
			<p align="center">
				<b>Are you sure you want to <?= $user->getSuspended() ? 'un' : '' ?>suspend this user?</b>
				<br>
			</p>

			<table>
				<tbody>
					<tr>
						<th>Email</th>
						<td><?= $user->getEmail(); ?></td>
					</tr>
					<tr>
						<th>Name</th>
						<td><?= $user->getNameFull(); ?></td>
					</tr>
				</tbody>
			</table>
			<br>
			<p align="center">
				<a
					class="waves-effect waves-light btn <?= $user->getSuspended() ? 'amber darken-3' : 'green accent-4'; ?>"
					href="./../View/?id=<?= $user->getId(); ?>">No</a> &nbsp; <a
					class="waves-effect waves-light btn <?= $user->getSuspended() ? 'green accent-4' : 'amber darken-3'; ?>"
					href="./?id=<?= $user->getId(); ?>&suspend&confirm">Yes</a>
			</p>
		<?php
    } else {
        $result = Intranet::getDbLink()->query('SELECT `id` FROM `users` ORDER BY `email` ASC;');
        foreach ($result->fetch_all() as $user) {
            $users[] = User::fromDatabase($user[0]);
        }
        ?>
			<h5>All Users</h5>
			<table>
				<thead>
					<tr>
						<th data-field="name"><small>Name <small>(Click to Inspect)</small></small></th>
						<th data-field="class"><small>Class</small></th>
						<th data-field="email"><small>Email <small>(Click to Email)</small></small></th>
					</tr>
				</thead>
				<tbody>
			<?php
        foreach ($users as $user) {
            ?>
					<tr>
						<td><a href="./../View/?id=<?= $user->getId(); ?>"
							class="valign-wrapper"><small><b><?= $user->getNameFull(); ?><?= !$user->getSuspended() ? '' : ' <font color="red">(Suspended)</font>'; ?></b></small></a></td>
						<td><small><?= ucfirst($user->getClass()); ?></small></td>
						<td><a href="mailto:<?= $user->getEmail(); ?>"
							title="Email <?= $user->getNameFull(); ?>"><small><?= $user->getEmail(); ?></small></a></td>
					</tr>
		<?php
        }
        ?>
				</tbody>
			</table>
			<br>
			<div align="center">
				<a class="btn waves-effect waves-light cyan lighten-2"
					href="./?create">Create User</a> &nbsp; <a
					class="btn waves-effect waves-light light-blue darken-3"
					href="<?= (isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : Config::getProperty('BaseDirWebserver'); ?>">Back</a>
				<!-- if strpos delete or suspend then http referer -->
			</div>
			<br>
	<?php
    }
    ?>	
		</div>
	</div>
	</main>
	<?php
    require_once (Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'style' . DIRECTORY_SEPARATOR . 'footer.php');
} else {
    header('Location: ' . Config::getProperty('BaseDirWebserver'));
}
