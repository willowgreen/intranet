<?php
require_once (__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'config.php');
if (in_array($_SESSION['user']->getClass(), Intranet::$GROUP_CONTRIBUTORS)) {
    if (isset($_GET['query']) && $_GET['query'] != '') {
        $search_full = $_GET['query'];
        $search_full = str_replace('"', '', $search_full);
        $search_split = explode(' ', $search_full);
        $search_onlyattributes = '';
        $search_noattributes = '';
        $attributes = array();
        $queries = array();
        foreach ($search_split as $word) {
            if (strpos($word, '=') !== false) {
                $attribute = substr($word, 0, strpos($word, '='));
                $value = substr($word, strpos($word, '=') + 1);
                switch (strtolower($attribute)) {
                    default:
                        break;
                    
                    case 'uploader':
                    case 'user':
                    case 'by':
                    case 'from':
                        if ($value == 'me') {
                            $value = $_SESSION['user']->getId();
                        }
                        if (strpos($value, '@') !== false) {
                            $result = Intranet::getDbLink()->query('SELECT `id` FROM `users` WHERE `email`=\'' . $value . '\';');
                            $value = @$result->fetch_all()[0][0];
                        }
                        if (! is_null($value) && $value != '') {
                            $queries[] = '(`uploader` LIKE \'%' . $value . '%\')';
                            $attributes['uploader'] = $value;
                        }
                        break;
                    
                    case 'date':
                    case 'on':
                    case 'uploaded':
                        $value = str_replace('today', '@' . time(), $value);
                        $value = str_replace('yesterday', '@' . strtotime('-1 day', time()), $value);
                        
                        switch (strtolower($value)) {
                            default:
                                break;
                            
                            case 'january':
                            case 'jan':
                                $value = date('Y') . '-01';
                                break;
                            
                            case 'february':
                            case 'feb':
                                $value = date('Y') . '-02';
                                break;
                            
                            case 'march':
                            case 'mar':
                                $value = date('Y') . '-03';
                                break;
                            
                            case 'april':
                            case 'apr':
                                $value = date('Y') . '-04';
                                break;
                            
                            case 'may':
                                $value = date('Y') . '-05';
                                break;
                            
                            case 'june':
                            case 'jun':
                                $value = date('Y') . '-06';
                                break;
                            
                            case 'july':
                            case 'jul':
                                $value = date('Y') . '-07';
                                break;
                            
                            case 'august':
                            case 'aug':
                                $value = date('Y') . '-08';
                                break;
                            
                            case 'september':
                            case 'sept':
                            case 'sep':
                                $value = date('Y') . '-09';
                                break;
                            
                            case 'october':
                            case 'oct':
                                $value = date('Y') . '-10';
                                break;
                            
                            case 'november':
                            case 'nov':
                                $value = date('Y') . '-11';
                                break;
                            
                            case 'december':
                            case 'dec':
                                $value = date('Y') . '-12';
                                break;
                        }
                        
                        if (strpos($value, '-') === false)
                            $value = date('Y-m-d', strtotime($value));
                        
                        $queries[] = '(`date` LIKE \'' . $value . '%\')';
                        $attributes['date'] = $value;
                        break;
                    
                    case 'id':
                    case 'guid':
                    case 'identifier':
                        $queries[] = '(`id` LIKE \'%' . $value . '%\')';
                        $attributes['id'] = $value;
                        break;
                    
                    case 'class':
                    case 'type':
                    case 'category':
                        $queries[] = '(`class` LIKE \'%' . $value . '%\')';
                        $attributes['class'] = $value;
                        break;
                }
            } else {
                $search_noattributes .= $word . ' ';
                // $queries [] = '(`name` LIKE \'%' . $word . '%\')';
            }
        }
        $search_full = $search_noattributes;
        foreach ($attributes as $attribute => $value) {
            $search_full .= $attribute . '=' . $value . ' ';
        }
        $search_full = rtrim(preg_replace('!\s+!', ' ', $search_full));
        if (isset($queries)) {
            array_unshift($queries, '(`name` LIKE \'%' . rtrim($search_noattributes) . '%\')');
            $queries = array_unique($queries);
        }
        
        $query = 'SELECT `id` FROM `documents` WHERE ';
        // var_dump ( $queries );
        for ($i = 0; $i < count($queries); $i ++) {
            $query .= $queries[$i];
            if ($i < count($queries) - 1)
                $query .= ' AND ';
            else
                $query .= ';';
        }
        // var_dump ( $query );
    }
    
    require_once (Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'style' . DIRECTORY_SEPARATOR . 'header.php');
    ?>
<body class="light-blue darken-4">
	<main class="light-blue darken-3"> <br>
	<div class="container">
		<div class="card-panel">
			<h4 align="center">Search Documents</h4>
			<?php if (!isset($search_full)) { ?><p>
				You can also use operators when searching. For example, including <small>uploader=employee@armourenergy.com.au</small>
				in your search will return documents uploaded by that user. <a
					href="<?= Config::getProperty('BaseDirWebserver'); ?>/Help/#SO">See
					the full list of operators here.</a>
			</p><?php } ?>
			<form action="./" method="GET">
				<br>
				<div align="center">
					<input id="query" name="query" type="text"
						placeholder="&quot;procedure 2017&quot;, &quot;uploader=rfarrington@armourenergy.com.au&quot;, &quot;type=procedure&quot;"
						value="<?= isset($search_full) ? $search_full : '' ?>" required>
					<button class="btn waves-effect waves-light cyan lighten-2"
						type="submit" name="submit">Search</button>
					<?php if (!isset($search_full)) { ?>&nbsp; <a
						class="btn waves-effect waves-light light-blue darken-3"
						href="./../Manage">Back</a><?php } ?>
				</div>
				<br>
			</form>
			<?php
    if (! empty($attributes)) {
        echo ('<br><div align="center">');
        foreach ($attributes as $attribute => $value) {
            ?>
			<div class="chip">
				<b><?= ucfirst($attribute) ?></b>/<?= $value ?><a
					href="./?query=<?= str_replace($attribute . '=' . $value, '', $search_full); ?>&submit"><i
					class="close material-icons" style="color: red;">close</i> </a>
			</div>
			<?php
        }
        echo ('</div>');
    }
    if (isset($search_full)) {
        echo ('<br><br>');
        $result = Intranet::getDbLink()->query($query);
        $documents = @$result->fetch_all();
        // $documents = array_unique ( $documents, SORT_REGULAR );
        // var_dump($documents);
        ?>
			<h4 align="center">Results</h4>
			<table>
				<thead>
					<tr>
						<th data-field="name">Name</th>
						<th data-field="class">Type</th>
						<th data-field="description">Description</th>
						<th data-field="uploader">Uploader</th>
						<th data-field="date">Uploaded</th>
						<th data-field="size">Size</th>
						<th data-field="view"></th>
						<th data-field="edit"></th>
						<th data-field="delete"></th>
					</tr>
				</thead>
				<tbody>
	
		<?php
        if (empty($documents)) {
            ?>
					<tr>
						<th colspan="9" style="text-align: center;">There are no
							documents.</th>
					</tr>
		<?php
        }
        
        foreach ($documents as $document) {
            $document = Document::fromDatabase($document[0]);
            ?>
					<tr>
						<td><small><b><?= $document->getName(); ?></b></small></td>
						<td><a href="./?query=class=<?= $document->getClass(); ?>"><small><?= ucfirst ( $document->getClass() ); ?></small></a></td>
						<td><small><?= $document->getDescription(); ?></small></td>
		<?php
            $uploader = User::fromDatabase($document->getUploader());
            ?>
						<td><a
							href="<?= Config::getProperty('BaseDirWebserver'); ?>/Users/View/?id=<?= $uploader->getId(); ?>"><small>
		<?= $uploader->getNameFull(); ?></small></a></td>
						<td><small><?= date('j F Y, g:i a', strtotime($document->getDate())); ?></small></td>
						<td><small><?php
            if (($size = filesize(realpath(Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'Documents' . DIRECTORY_SEPARATOR . 'Files' . DIRECTORY_SEPARATOR . $document->getId()))) > 1000000) {
                $size = $size / 1024 / 1024;
                $type = 'MiB';
            } else {
                $size = $size / 1024;
                $type = 'KiB';
            }
            echo (number_format($size, 2) . ' ' . $type);
            ?></small></td>
						<td style="text-align: center;"><a
							href="./../View/?id=<?= $document->getId(); ?>"><small>view</small></a></td>
						<td style="text-align: center;"><a
							href="./../Edit/?id=<?= $document->getId(); ?>"><small>edit</small></a></td>
						<td style="text-align: center;"><a
							href="./../Delete/?id=<?= $document->getId(); ?>"><small>delete</small></a></td>
					</tr>			
	<?php
        }
        ?>
				</tbody>
			</table>
			<p align="center">
				<a class="btn waves-effect waves-light light-blue darken-3"
					href="./">Back</a>
			</p>
	<?php
    }
    ?>
		</div>
	</div>
	</main>
	<?php
    require_once (Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'style' . DIRECTORY_SEPARATOR . 'footer.php');
} else {
    header('Location: ' . Config::getProperty('BaseDirWebserver'));
}
