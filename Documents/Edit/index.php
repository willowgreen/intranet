<?php
require_once (__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'config.php');
if (in_array($_SESSION['user']->getClass(), Intranet::$GROUP_ADMINISTRATORS)) {
    require_once (Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'style' . DIRECTORY_SEPARATOR . 'header.php');
    ?>
<body class="light-blue darken-4">
	<main class="light-blue darken-3"> <br>
	<div class="container">
		<div class="card-panel">
			<h4 align="center">Edit Document</h4>
	<?php
    if (! isset($_GET['id'])) {
        @header('Location: ./../Manage');
        echo ('<meta http-equiv="refresh" content="0; url=./../Manage">');
        die();
    }
    $id = $_GET['id'];
    $document = Document::fromDatabase($id);
    
    if (isset($_GET['success'])) {
        echo ('<p style="font-size: 14px; text-align: center;" class="green-text">Your document <i>' . $document->getName() . '</i> was successfully uploaded. <a href="./../Manage">Click here to go back.</a></p><p style="margin-bottom: 30px;"></p>');
    }
    
    if (isset($_POST['submit'])) {
        $class = $_POST['class'] == '' ? 'document' : $_POST['class'];
        if ($post != $document->getClass()) {
            Intranet::getDbLink()->query('UPDATE `documents` SET `class`=\'' . Intranet::getDbLink()->real_escape_string($class) . '\' WHERE `id`=\'' . $document->getId() . '\';');
        }
        
        $name = $_POST['name'];
        if ($name != $document->getName()) {
            Intranet::getDbLink()->query('UPDATE `documents` SET `name`=\'' . Intranet::getDbLink()->real_escape_string($name) . '\' WHERE `id`=\'' . $document->getId() . '\';');
        }
        
        $description = $_POST['description'];
        if ($description != $document->getDescription()) {
            Intranet::getDbLink()->query('UPDATE `documents` SET `description`=\'' . Intranet::getDbLink()->real_escape_string($description) . '\' WHERE `id`=\'' . $document->getId() . '\';');
        }
        @header('Location: ./../Manage');
        echo ('<meta http-equiv="refresh" content="0; url=./../Manage">');
        die();
    }
    ?>
			<form action="<?= basename($_SERVER['REQUEST_URI']); ?>"
				method="POST">
				<div class="input-field">
					<input id="name" name="name" type="text" class="validate"
						value="<?= $document->getName(); ?>"> <label class="active"
						for="name">Document Name</label>
				</div>
				<div class="input-field">
					<select class="browser-default" id="class" name="class">
						<option value="document" disabled>Document Class</option>
						<option value="document"
							<?= $document->getClass() == 'document' ? ' selected' : ''; ?>>General
							Document</option>
						<option value="procedure"
							<?= $document->getClass() == 'procedure' ? ' selected' : ''; ?>>Procedure</option>
						<option value="policy"
							<?= $document->getClass() == 'policy' ? ' selected' : ''; ?>>Policy</option>
						<option value="form"
							<?= $document->getClass() == 'form' ? ' selected' : ''; ?>>Form</option>
					</select>
				</div>
				<br>
				<div class="input-field">
					<textarea id="description" name="description"
						class="materialize-textarea validate"><?= $document->getDescription(); ?></textarea>
					<label for="description">Document Description</label>
				</div>
				<br> <br>
				<div align="center">
					<button class="btn waves-effect waves-light cyan lighten-2"
						type="submit" name="submit">Submit</button>
					&nbsp; <a class="btn waves-effect waves-light light-blue darken-3"
						href="<?php
    if ((isset($_SERVER['HTTP_REFERER'])) && (strpos($_SERVER['HTTP_REFERER'], '/Add') === false) || strpos($_SERVER['HTTP_REFERER'], '/View') === false || ! isset($_SERVER['HTTP_REFERER'])) {
        echo (Config::getProperty('BaseDirWebserver') . '/Documents/Manage');
    } else if (isset($_SERVER['HTTP_REFERER'])) {
        echo ($_SERVER['HTTP_REFERER']);
    }
    ?>
">Back</a>

				</div>
			</form>
		</div>
	</div>
	</main>
<?php
    require_once (Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'style' . DIRECTORY_SEPARATOR . 'footer.php');
} else {
    header('Location: ' . Config::getProperty('BaseDirWebserver'));
}
