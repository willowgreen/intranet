<?php
require_once (__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'config.php');
if (in_array($_SESSION['user']->getClass(), Intranet::$GROUP_CONTRIBUTORS)) {
    if (! isset($_GET['id'])) {
        header('Location: ./../Manage');
    }
    
    $id = $_GET['id'];
    require_once (Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'style' . DIRECTORY_SEPARATOR . 'header.php');
    ?>
<body class="light-blue darken-4">
	<main class="light-blue darken-3"> <br>
	<div class="container">
		<div class="card-panel">
			<h4 align="center">Delete</h4>
	<?php
    if (isset($_GET['confirm'])) {
        $filepath = Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'Documents' . DIRECTORY_SEPARATOR . 'Files' . DIRECTORY_SEPARATOR . $id;
        if (is_file($filepath)) {
            unlink($filepath);
        }
        Intranet::getDbLink()->query('DELETE FROM `documents` WHERE `id`=\'' . $id . '\';');
        @header('Location: ./../Manage');
        echo ('<meta http-equiv="refresh" content="0; url=./../Manage">');
        die();
    }
    
    $document = Document::fromDatabase($id);
    $user = User::fromDatabase($document->getUploader());
    ?>
			<p align="center">
				<b>Are you sure you want to delete this record?</b> <br>
			</p>

			<table>
				<tbody>
					<tr>
						<th>Name</th>
						<td><?= $document->getName(); ?></td>
					</tr>
					<tr>
						<th>Filename</th>
						<td><?= $document->getFilename(); ?></td>
					</tr>
					<tr>
						<th>Uploader</th>
						<td><a
							href="<?= Config::getProperty('BaseDirWebserver'); ?>/Users/View/?id=<?= $user->getId(); ?>"><?= $user->getNameFull(); ?></a></td>
					</tr>
					<tr>
						<th>Date</th>
						<td><?= $document->getDate(); ?></td>
					</tr>
				</tbody>
			</table>
			<br>
			<p align="center">
				<a class="waves-effect waves-light btn green accent-4"
					href="./../Manage">No</a> &nbsp; <a
					class="waves-effect waves-light btn amber darken-3"
					href="./?id=<?= $document->getId(); ?>&delete&confirm">Yes</a>
			</p>
		</div>
	</div>
	</main>
<?php
    require_once (Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'style' . DIRECTORY_SEPARATOR . 'footer.php');
}
