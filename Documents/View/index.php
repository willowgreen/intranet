<?php
require_once (__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'config.php');
{
    $id = $_GET['id'];
    $document = Document::fromDatabase($id);
    $file = Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'Documents' . DIRECTORY_SEPARATOR . 'Files' . DIRECTORY_SEPARATOR . $document->getId();
    
    header('Content-description: File Transfer');
    header('Content-type: ' . $document->getMimeType());
    header('Content-disposition: inline; filename="' . $document->getFilename() . '"'); // 'inline' refers to showing the attachment in-browser if possible
    header('Expires: 0');
    header('Cache-control: must-revalidate');
    header('Pragma: public');
    header('Content-length: ' . filesize($file));
    readfile($file); // output the file's contents to the client
}
