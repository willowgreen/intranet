<?php
require_once (__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'config.php');
if (in_array($_SESSION['user']->getClass(), Intranet::$GROUP_CONTRIBUTORS)) {
    require_once (Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'style' . DIRECTORY_SEPARATOR . 'header.php');
    ?>
<body class="light-blue darken-4">
	<main class="light-blue darken-3"> <br>
	<div class="container">
		<div class="card-panel">
			<h4 align="center">Manage Documents</h4>
			<br>
			<div align="center">
				<a href="<?= Config::getProperty('BaseDirWebserver'); ?>"><i
					class="material-icons small">chevron_left</i></a> &nbsp; <a
					href="./../Search"><i class="material-icons small">search</i></a>
				&nbsp; <a href="./../Add"><i class="material-icons small">note_add</i></a>
			</div>	
	<?php
    $result = Intranet::getDbLink()->query('SELECT `id` FROM `documents` ORDER BY `date` DESC;');
    $documents = $result->fetch_all();
    ?>
			<h5>All Documents</h5>
			<br>
			<table>
				<thead>
					<tr>
						<th data-field="name"><small>Name <small>(Click to View)</small></small></th>
						<th data-field="class"><small>Type <small>(Click to Browse This
									Type)</small></small></th>
						<th data-field="description"><small>Description <small>(40
									character extract)</small></small></th>
						<th data-field="uploader"><small>Uploader <small>(Click to View
									Uploads)</small></small></th>
						<th data-field="date"><small>Last Updated</small></th>
						<th data-field="size"><small>Size</small></th>
						<th data-field="edit" width="3%"></th>
						<th data-field="delete" width="3%"></th>
					</tr>
				</thead>
				<tbody>
	
		<?php
    if (empty($documents)) {
        ?>
					<tr>
						<th colspan="9" style="text-align: center;">There are no
							documents.</th>
					</tr>
		<?php
    }
    
    foreach ($documents as $document) {
        $document = Document::fromDatabase($document[0]);
        ?>
					<tr>
						<td><small><a href="./../View/?id=<?= $document->getId(); ?>"
								class="valign-wrapper"><b><?= $document->getName(); ?></b></a></small></td>
						<td><a
							href="./../Search/?query=class=<?= $document->getClass(); ?>"><small><?= ucfirst ( $document->getClass() ); ?></small></a></td>
						<td><small><?= substr($document->getDescription(), 0, 40 - 1); ?></small></td>
		<?php $uploader = User::fromDatabase ( $document->getUploader () ); ?>
						<td><a class="tooltipped" data-position="top" data-delay="50"
							data-tooltip="<?= $uploader->getEmail(); ?>"
							href="<?= Config::getProperty('BaseDirWebserver'); ?>/Users/View/?id=<?= $uploader->getId(); ?>"><small>
		<?= $uploader->getNameFull(); ?></small></a></td>
						<td><small><?= date('j F Y, g:i a', strtotime($document->getDate())); ?></small></td>
						<td><small><?php
        $size = filesize(realpath(Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'Documents' . DIRECTORY_SEPARATOR . 'Files' . DIRECTORY_SEPARATOR . $document->getId()));
        if ($size / 1024 / 1024 > 1) {
            $type = 'MiB';
        } else {
            $size = $size / 1024;
            $type = 'KiB';
        }
        echo (number_format($size, 2) . ' ' . $type);
        ?></small></td>
						<td style="text-align: center;"><a
							href="./../Edit/?id=<?= $document->getId(); ?>"> <!-- <small>edit</small> -->
								<i class="material-icons tiny">mode_edit</i>
						</a></td>
						<td style="text-align: center;"><a
							href="./../Delete/?id=<?= $document->getId(); ?>"> <!-- <small>delete</small> -->
								<i class="material-icons tiny">delete</i>
						</a></td>
					</tr>
	<?php
    }
    ?>
				</tbody>
			</table>
			<br> <br>
			<div align="center">
				<a href="<?= Config::getProperty('BaseDirWebserver'); ?>"><i
					class="material-icons small">chevron_left</i></a> &nbsp; <a
					href="./../Search"><i class="material-icons small">search</i></a>
				&nbsp; <a href="./../Add"><i class="material-icons small">note_add</i></a>
			</div>
		</div>
	</div>
	</main>
	<?php
    require_once (Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'style' . DIRECTORY_SEPARATOR . 'footer.php');
} else {
    header('Location: ' . Config::getProperty('BaseDirWebserver'));
}
