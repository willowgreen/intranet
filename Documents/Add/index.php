<?php
require_once (__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'config.php');
if (in_array($_SESSION['user']->getClass(), Intranet::$GROUP_CONTRIBUTORS)) {
    
    if (isset($_POST['submit'])) {
        $id = GUID();
        $filename = $_FILES['file']['name'];
        $mimetype = $_FILES['file']['type'];
        $class = ! isset($_POST['class']) || $_POST['class'] == '' ? 'document' : $_POST['class'];
        $name = $_POST['name'];
        $description = $_POST['description'];
        
        if ($_FILES['file']['name'] != '') {
            $filepath = Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'Documents' . DIRECTORY_SEPARATOR . 'Files' . DIRECTORY_SEPARATOR . $id;
            if (file_exists($filepath)) {
                echo ('Document already exists!');
            } else {
                Intranet::getDbLink()->query('INSERT INTO `documents` (`id`, `filename`, `mimetype`, `class`, `name`, `description`, `date`, `uploader`) VALUES (\'' . $id . '\', \'' . $filename . '\', \'' . Intranet::getDbLink()->real_escape_string($mimetype) . '\', \'' . Intranet::getDbLink()->real_escape_string($class) . '\', \'' . Intranet::getDbLink()->real_escape_string(htmlentities($name)) . '\', \'' . Intranet::getDbLink()->real_escape_string(htmlentities($description)) . '\', CURRENT_TIMESTAMP, \'' . $_SESSION['user']->getId() . '\')');
                move_uploaded_file($_FILES['file']['tmp_name'], $filepath);
                @header('Location: ./../Edit/?success&id=' . $id);
                echo ('<meta http-equiv="refresh" content="0; url=./../Edit/?success&id=' . $id . '">');
                die();
            }
        }
    }
    
    require_once (Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'style' . DIRECTORY_SEPARATOR . 'header.php');
    ?>
<body class="light-blue darken-4">
	<main class="light-blue darken-3"> <br>
	<div class="container">
		<div class="card-panel">
			<h4 align="center">Add Document</h4>
			<form action="./" method="POST" enctype="multipart/form-data">
				<div class="input-field">
					<input id="name" name="name" type="text" class="validate"> <label
						class="active" for="name">Document Name</label>
				</div>
				<div class="input-field">
					<select class="browser-default" id="class" name="class">
						<option value="document" disabled selected>Document Class</option>
						<option value="document">General Document</option>
						<option value="procedure">Procedure</option>
						<option value="policy">Policy</option>
						<option value="form">Form</option>
					</select>
				</div>
				<br>
				<div class="input-field">
					<textarea id="description" name="description"
						class="materialize-textarea validate"></textarea>
					<label for="description">Document Description</label>
				</div>
				<div class="file-field input-field">
					<div class="btn light-blue darken-3">
						<span>File</span> <input type="file" id="file" name="file">
					</div>
					<div class="file-path-wrapper">
						<input class="file-path validate" type="text">
					</div>
				</div>
				<br> <br>
				<div align="center">
					<button class="btn waves-effect waves-light cyan lighten-2"
						type="submit" name="submit">Submit</button>
					&nbsp; <a class="btn waves-effect waves-light light-blue darken-3"
						href="<?= isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : './../Manage'; ?>">Back</a>
				</div>
				<br>
			</form>
		</div>
	</div>
	</main>
<?php
    require_once (Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'style' . DIRECTORY_SEPARATOR . 'footer.php');
}
