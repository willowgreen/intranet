<?php
if (! defined('LOGIN') && ! defined('INDUCTION_VIEW')) {
    ?>
<footer class="page-footer light-blue darken-4">
	<div class="container white-text">
		<div class="row">
			<font size="2">
			Logged in as <?= $_SESSION['user']->getNameFull(); ?>.
			<br>
			&copy; 2016&mdash;<?= date('Y', time()) ?> <?= Config::getProperty('SiteOwner') ?>, all rights reserved.
			<?php if (Config::getProperty('Debug')) { ?>
				<br>Intranet solution by Nicholas Schlobohm <a
				href="https://t.me/nicholasschlobohm">(contact)</a> &mdash; version <?= Intranet::getVersion() . ' § ' . substr(Intranet::getVersionHash(), 0, 7) . '.' . substr(Intranet::getLatestRevision(), 0, 7); ?>
				<?php
        $NUMQUERIES_END = Intranet::getDbNumQueries();
        $NUMQUERIES_TOTAL = $NUMQUERIES_END - $NUMQUERIES_BEGINNING;
        echo (' | ' . $NUMQUERIES_TOTAL . ' SQL queries executed.<br>');
        if (isset($_SESSION['login']) && $_SESSION['login']) {
            echo ('<a href="' . Config::getProperty('BaseDirWebserver') . '/System/Manage" class="waves-effect" title="Configure the intranet" style="color: rgb(183, 253, 255);">Click here to manage system</a>');
        }
        ?>
        </font>
		</div>
	</div>
	<?php } ?>
</footer>
<?php
}
?>
<script type="application/javascript">
<?php
if (! defined('LOGIN') && ! defined('INDUCTION_VIEW')) {
    ?>
$('.datepicker').pickadate({
    selectMonths: true, // dropdown for months
    selectYears: 15, // dropdown of 15 years
    today: 'Today',
    clear: 'Clear',
    close: 'Ok',
    closeOnSelect: false
  });
<?php
    if (! isset($_SESSION['WELCOME_TOAST']) || ! $_SESSION['WELCOME_TOAST']) {
        ?>
        Materialize.toast('Welcome, <?= $_SESSION['user']->getNameFirst(); ?>.', 4000);
		<?php
        $_SESSION['WELCOME_TOAST'] = true;
    }
}
?>
</script>
</body>
<?php

if (Config::getProperty('Debug')) {
    ?>
<script type="text/javascript">
    Justice.init();
</script>
<?php
}
?>
</html>