<!-- 
<?= Config::getProperty('SiteTitle'); ?> uses an intranet framework developed by Nicholas Schlobohm
<?= (isset($_SESSION['login']) && $_SESSION['login'] ? 'Logged in as '. $_SESSION['user']->getNameFull() . '.' : 'Not logged in.') . PHP_EOL; ?>
<?= Intranet::getVersion() . ' § ' . substr(Intranet::getVersionHash(), 0, 7) . '.' . substr(Intranet::getLatestRevision(), 0, 7) . PHP_EOL; ?>
-->

<?php
$md = new Mobile_Detect();
$isMobile = ($md->isMobile() || $md->isTablet()) || (Config::getProperty('Debug')) && isset($_GET['mobile']);
if ($isMobile)
    define('LOGIN', 1);
?>

<!DOCTYPE html>
<html lang="en">

<script type="application/javascript">
console.log("<?= Config::getProperty('SiteTitle') . ': ' . Intranet::getVersion() . ' § ' . substr(Intranet::getVersionHash(), 0, 7) . '.' . substr(Intranet::getLatestRevision(), 0, 7); ?>");
</script>

<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
<title><?= Config::getProperty('SiteOwner') . ' - ' . Config::getProperty('SiteTitle') ?></title>
<link href="https://fonts.googleapis.com/css?family=Inconsolata"
	rel="stylesheet" type="text/css">
<?php
echo ('<style type="text/css">
/* fallback */
@font-face {
  font-family: \'Material Icons\';
  font-style: normal;
  font-weight: 400;
  src: url(' . Config::getProperty('BaseDirWebserver') . '/style/material-icons.woff2) format(\'woff2\');
}

.material-icons {
  font-family: \'Material Icons\';
  font-weight: normal;
  font-style: normal;
  font-size: 24px;
  line-height: 1;
  letter-spacing: normal;
  text-transform: none;
  display: inline-block;
  white-space: nowrap;
  word-wrap: normal;
  direction: ltr;
  -webkit-font-feature-settings: \'liga\';
  -webkit-font-smoothing: antialiased;
}
</style>');
?>
<link rel="stylesheet"
	href="<?= Config::getProperty('BaseDirWebserver'); ?>/style/css/materialize.css">
<style type="text/css">
body {
	display: flex;
	min-height: 100vh;
	flex-direction: column;
}

main {
	flex: 1 0 auto;
}
</style>
<?= !defined('LOGIN') ? '<style type="text/css">body { margin-left: 300px; }</style>' . PHP_EOL : ''; ?>
<?php

$l = ! defined('LOGIN');
echo '<style type="text/css">
/* label color */
.input-field label {
	font-weight: thin;
	color: #' . ($l ? '039be5' : 'fff') . ';
}
/* label focus color */
.input-field input[type=text]:focus+label {
	color: #' . ($l ? '039be5' : 'fff') . ' !important;
}
/* label underline focus color */
.input-field input[type=text]:focus {
	border-bottom: 1px solid #' . ($l ? '039be5' : 'fff') . ';
	box-shadow: 0 1px 0 0 #' . ($l ? '039be5' : 'fff') . ' !important;
}
/* label focus color */
.input-field input[type=password]:focus+label {
	color: #<' . ($l ? '039be5' : 'fff') . ' !important;
}
/* label underline focus color */
.input-field input[type=password]:focus {
	border-bottom: 1px solid #' . ($l ? '039be5' : 'fff') . ';
	box-shadow: 0 1px 0 0 #' . ($l ? '039be5' : 'fff') . ' !important;
}

.dropdown-content li > a, .dropdown-content li > span {
  font-size: 16px;
  color: #039be5;
  display: block;
  line-height: 22px;
  padding: 14px 16px;
}
</style>
<style type="text/css">
[type="radio"]:checked + label:after,
[type="radio"].with-gap:checked + label:after {
  background-color: #039be5;
}

[type="radio"]:checked + label:after,
[type="radio"].with-gap:checked + label:before,
[type="radio"].with-gap:checked + label:after {
  border: 2px solid #039be5;
}

blockquote {
  border-left: 5px solid #' . ($l ? '039be5' : 'fff') /*  !important */ . ';
}

input:not([type]):focus:not([readonly]),
input[type=text]:not(.browser-default):focus:not([readonly]),
input[type=password]:not(.browser-default):focus:not([readonly]),
input[type=email]:not(.browser-default):focus:not([readonly]),
input[type=url]:not(.browser-default):focus:not([readonly]),
input[type=time]:not(.browser-default):focus:not([readonly]),
input[type=date]:not(.browser-default):focus:not([readonly]),
input[type=datetime]:not(.browser-default):focus:not([readonly]),
input[type=datetime-local]:not(.browser-default):focus:not([readonly]),
input[type=tel]:not(.browser-default):focus:not([readonly]),
input[type=number]:not(.browser-default):focus:not([readonly]),
input[type=search]:not(.browser-default):focus:not([readonly]),
textarea.materialize-textarea:focus:not([readonly]) {
  border-bottom: 1px solid #' . ($l ? '039be5' : 'fff') . ';
  -webkit-box-shadow: 0 1px 0 0 #' . ($l ? '039be5' : 'fff') . ';
          box-shadow: 0 1px 0 0 #' . ($l ? '039be5' : 'fff') . ';
}

input:not([type]):focus:not([readonly]) + label,
input[type=text]:not(.browser-default):focus:not([readonly]) + label,
input[type=password]:not(.browser-default):focus:not([readonly]) + label,
input[type=email]:not(.browser-default):focus:not([readonly]) + label,
input[type=url]:not(.browser-default):focus:not([readonly]) + label,
input[type=time]:not(.browser-default):focus:not([readonly]) + label,
input[type=date]:not(.browser-default):focus:not([readonly]) + label,
input[type=datetime]:not(.browser-default):focus:not([readonly]) + label,
input[type=datetime-local]:not(.browser-default):focus:not([readonly]) + label,
input[type=tel]:not(.browser-default):focus:not([readonly]) + label,
input[type=number]:not(.browser-default):focus:not([readonly]) + label,
input[type=search]:not(.browser-default):focus:not([readonly]) + label,
textarea.materialize-textarea:focus:not([readonly]) + label {
  color: #' . ($l ? '039be5' : 'fff') . ';
}
</style>';
?>
<script
	src="<?= Config::getProperty('BaseDirWebserver'); ?>/style/js/jquery.js"></script>
<script
	src="<?= Config::getProperty('BaseDirWebserver'); ?>/style/js/materialize.js"></script>
<script
	src="<?= Config::getProperty('BaseDirWebserver'); ?>/style/js/justice.js"></script>
</head>

<?php
if ($isMobile) {
    ?>
<body class="light-blue darken-2">
	<br>
	<br>
	<div class="container">
	<?= Config::getProperty('SiteUseLogo') ? '<br><img style="float: right; max-width: 290px;" src="data:image/png;base64,' . Intranet::getLogoWhite_base64() . '">' : '<h4 align="center">' . Config::getProperty('SiteTitle') . '</h4>'?>
	<br> <br>
		<h2 class="white-text" style="font-weight: 100;">Oops</h2>
		<br> <br> <br>
		<h4 class="thin white-text">Sorry. <?= Config::getProperty('SiteTitle'); ?> doesn't support mobile devices. For the best experience, please access the site from a desktop computer.</h4>
	</div>
</body>  
    <?php
    require_once (Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'style' . DIRECTORY_SEPARATOR . 'footer.php');
    $_SESSION['login'] = false;
    unset($_SESSION['login']);
    die();
}
?>

<?php
if (! defined('LOGIN') && ! defined('INDUCTION_VIEW')) {
    ?>
<header>
	<div class="container">
		<a href="#" data-activates="nav-mobile"
			class="button-collapse top-nav waves-effect circle hide-on-large-only"><i
			class="material-icons small">menu</i></a>
	</div>
	<ul id="nav-mobile" class="side-nav fixed light-blue darken-1">
		<li class="logo">
	<?php
    if (Config::getProperty('SiteUseLogo')) {
        echo ('<br style="display: block; margin: 10px 0;"><div align="center"><img src="data:image/png;base64,' . Intranet::getLogoWhite_base64() . '" style="max-width: 200px;"></div>');
    } else {
        echo ('<h4 align="center">' . Config::getProperty('SiteTitle') . '</h4>');
    }
    ?>
	</li>
		<li><a
			href="<?= Config::getProperty('BaseDirWebserver') . '/Users/View/Me'; ?>"
			title="My details"><span class="thin white-text"
				style="font-size: 20px; text-align: center; margin: auto; display: block; margin-top: 10px;"><?= $_SESSION['user']->getNameFull(); ?></span></a>
			<a class="waves-effect white-text"
			style="margin-top: 15px; font-weight: bold;"
			href="<?= Config::getProperty('BaseDirWebserver') . '/Users/Auth/Logout'; ?>">Log
				out</a></li>
		<li class="bold"><a
			href="<?= Config::getProperty('BaseDirWebserver'); ?>"
			class="waves-effect white-text">Home</a></li>
	<?php foreach (Intranet::getNavbar() as $category => $contents) { ?>
    <li class="no-padding">
			<ul class="collapsible collapsible-accordion light-blue darken-1">
				<li class="bold"><a
					class="collapsible-header waves-effect white-text"><?= $category; ?></a>
					<div class="collapsible-body light-blue accent-2">
						<ul>
    <?php
        foreach ($contents as $item => $attributes) {
            $attributes['link'] = str_replace('%basedir%', Config::getProperty('BaseDirWebserver'), $attributes['link']);
            $attributes['link'] = str_replace('%userid%', $_SESSION['user']->getId(), $attributes['link']);
            ?>
    <li><a class="waves-effect white-text" style="font-weight: bold;"
								href="<?= $attributes['link']; ?>"><?= $item; ?></a></li>
    <?php
            unset($item);
            unset($attributes);
        }
        ?>
            				</ul>
					</div></li>
			</ul>
		</li>
            <?php
    }
    ?>
	</ul>
</header>
<?php
}
?>
