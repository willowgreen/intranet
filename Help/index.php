<?php
require_once (__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'config.php');
require_once (Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'style' . DIRECTORY_SEPARATOR . 'header.php');
?>
<body>
	<main> <br>
	<div class="container">
		<div class="card-panel">
			<h4>Help</h4>
			<hr>
			<a id="EG" name="EG"></a>
			<h5>Email</h5>
			<a id="EG.1" name="EG.1" style="padding-left: 0em"><small><b>EG.1</b></small></a>
			The intranet system can send users and administrators emails. This
			can be useful for document expiry warnings and notifications, and
			induction results. <br> <a id="EG.1.1" name="EG.1.1"
				style="padding-left: 1em"><small><b>EG.1.1</b></small></a> It is
			important to stress that the service runs through Google's SMTPS
			servers, and therefore can only send 99 emails per day.
			<hr>
			<a id="SG" name="SG"></a>
			<h5>Searching</h5>
			<a id="SG.1" name="SG.1" style="padding-left: 0em"><small><b>SG.1</b></small></a>
			The intranet system allows for the searching of documents and
			training/licence records. This makes for a quicker workflow,
			especially when using the <a href="#SO">search operators</a> outlined
			below. <br> <a id="SG.2" name="SG.2" style="padding-left: 0em"><small><b>SG.2</b></small></a>
			To search for documents, either go to <a
				href="<?= Config::getProperty('BaseDirWebserver'); ?>/Documents/Search">document_search.php</a>
			or click the search button from the <a
				href="<?= Config::getProperty('BaseDirWebserver'); ?>/Documents/Manage">document
				management page</a>.
			<hr>
			<a id="SO" name="SO"></a>
			<h5>Search operators</h5>
			<?php
$search_operators = new SimpleXMLElement(file_get_contents(__DIR__ . DIRECTORY_SEPARATOR . 'Content' . DIRECTORY_SEPARATOR . 'Search' . DIRECTORY_SEPARATOR . 'operators.xml'));

$i = 0;
foreach ($search_operators->row as $operator) {
    $i ++;
    ?>
				<a id="SO.<?= $i ?>" name="SO.<?= $i ?>" style="padding-left: 0em"><small><b>SO.<?= $i ?></b></small></a>
				<?= $operator->attribute ?><br> <a id="SO.<?= $i ?>.1"
				name="SO.<?= $i ?>.1" style="padding-left: 1em"><small><b>SO.<?= $i ?>.1</b></small></a>
			<small><b>Example(s)</b>: <?= $operator->examples ?></small><br> <a
				id="SO.<?= $i ?>.2" name="SO.<?= $i ?>.2" style="padding-left: 1em"><small><b>SO.<?= $i ?>.2</b></small></a>
			<small><b>Alias(es)</b>: <?= $operator->aliases ?></small><br> <a
				id="SO.<?= $i ?>.3" name="SO.<?= $i ?>.3" style="padding-left: 1em"><small><b>SO.<?= $i ?>.3</b></small></a>
			<small><b>Valid for</b>: <?= $operator->validfor ?></small><br> <a
				id="SO.<?= $i ?>.4" name="SO.<?= $i ?>.4" style="padding-left: 1em"><small><b>SO.<?= $i ?>.5</b></small></a>
			<small><?= $operator->description; ?></small><br> <br>	
				<?php
}
unset($i);
?>
		</div>
	</div>
	</main>
<?php
require_once (Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'style' . DIRECTORY_SEPARATOR . 'footer.php');
