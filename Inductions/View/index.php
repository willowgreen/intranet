<?php
define('INDUCTION_VIEW', 1);
require_once (__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'config.php');

if (! isset($_GET['id']))
    header('Location: ./../Manage');
else
    $id = $_GET['id'];

$result = InductionResult::fromDatabase($id);
$user = User::fromDatabase($result->getRecipient());
$quiz = InductionDocument::fromFile($result->getDocNumber());

$url = 'http' . ((isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off') ? 's' : '') . '://' . $_SERVER['HTTP_HOST'] . strtok($_SERVER['REQUEST_URI'], '?') . '?id=' . $result->getId();
?>
<html>
<head>
<title><?= $quiz->getTitle() . ' | ' . $user->getNameFull(); ?></title>
<link rel="stylesheet"
	href="<?= Config::getProperty('BaseDirWebserver'); ?>/style/css/materialize.css">
<?php
if (! isset($_GET['id'])) {
    header('Location: ./../Manage');
}
if (isset($_GET['print'])) {
    ?>
<style type="text/css" media="print">
@page {
	size: landscape;
}
</style>
<script type="text/javascript">
	window.print();
</script>
	<?php
}
?>
</head>
<body>
	<div class="container">
		<div align="left">
			<br> <a href="<?= Config::getProperty('BaseDirWebserver'); ?>"><img
				alt="<?= Config::getProperty('SiteOwner'); ?>"
				src="<?= 'data:image/png;base64,' . Intranet::getLogo_base64(); ?>"
				style="display: inline; max-width: 170px;"></a>
		</div>
		<div align="center" style="margin-top: 10px;">
			<div align="center" style="margin-top: -48px; margin-bottom: 20px;">
				<font size="5"><?= $quiz->getTitle(); ?></font> <br> <small><?= $result->getDocNumber(); ?></small>
			</div>
		</div>
		<?php
if (! isset($_GET['print'])) {
    echo ('<br><p align="center"><a href="./?id=' . $result->getId() . '&print" target="_blank">Print This Record</a></p>');
}
?>
		<table>
			<tbody>
				<tr>
					<th>Recipient</th>
					<td><?= $user->getNameFull(); ?></td>
				</tr>
				<tr>
					<th>Effective Date</th>
					<td><?= date ( 'j F Y', strtotime ( $result->getDate () ) ); ?></td>
				</tr>
				<tr>
					<th>Valid Until</th>
					<td><?= $result->getValidUntil(); ?></td>
				</tr>
				<tr>
					<th>Pass/Fail</th>
					<td><?= ($result->getPass() ? '<font color="green">Pass (' : '<font color="red">Fail (') . $result->getCorrect () . '/' . $quiz->getQuestionsCount () . ', ' . $quiz->getCorrectRequired () . ' required)</font>' ?></td>
				</tr>
				<?php
    if (null !== $result->getContact() && $result->getContact() != '') {
        ?>
				<tr>
					<th>Armour Contact</th>
					<td><?= $result->getContact(); ?></td>
				</tr>
					<?php
    }
    ?>
			</tbody>
		</table>
		<?php
if (isset($_GET['print'])) {
    ?>
		<br>
		<div class="row">
		<?php
    if ($user->getPictureType() == User::PICTURE_FILE) {
        ?>
    <div class="col s3">
				<img src="<?= $user->getPictureImgContent(); ?>"
					alt="<?= $user->getNameFull(); ?>'s picture"
					style="img-align: left; max-height: 300px; display: inline;">
			</div>
			<div class="col s9">
					 <?php
    } else
        echo '<div class="col s12>"' . PHP_EOL;
    ?>
				<p align="left">

					<b>I, <i><?= $user->getNameFull(); ?></i>, acknowledge that I have
						personally read and understood the induction, successfully
						answered the questionnaire and agree to abide by all the
						requirements outlined in the induction.
					</b>
				</p>
				<br>
				<div align="center">
					<table>
						<tr>
							<td>Signed</td>
							<td width="80%">______________________________</td>
						</tr>
					</table>
					<br> <br>
					<table>
						<tr>
							<td>Date</td>
							<td width="80%">______________________________</td>
						</tr>
					</table>
				</div>
			</div>
		</div>
			<?php
}
?>
	</div>
</body>
</html>
