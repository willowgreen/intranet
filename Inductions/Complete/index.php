<?php
require_once (__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'config.php');
require_once (Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'style' . DIRECTORY_SEPARATOR . 'header.php');
?>
<body class="light-blue darken-4">
	<main class="light-blue darken-3" style="margin-bottom: -40px;"> <br>
	<div class="container">
		<div class="card-panel">
<?php
if (in_array($_SESSION['user']->getClass(), Intranet::$GROUP_INDUCTEES)) {
    if (! isset($_GET['doc'])) {
        $all = InductionDocument::getAllFilenames();
        ?>
		<h4 align="center">Complete an induction</h4>
			<form action="./" method="GET">
				<div class="row">
					<div align="center" class="input-field col s8">
						<input id="doc" name="doc" type="text" class="autocomplete"> <label
							for="search">Enter an induction document number</label>
					</div>
					<div align="right" class="input-field col s4">
						<button class="btn waves-effect waves-light light-blue darken-3"
							type="submit">Complete</button>
					</div>
				</div>
			</form>
			<script type="text/javascript">
    $(document).ready(function() {
        $('input.autocomplete').autocomplete({
            data: {
                <?php
        for ($i = 0; $i < count($all); $i ++) {
            echo ('"' . $all[$i] . '": null');
            if ($i + 1 != count($all))
                echo (',');
            echo (PHP_EOL);
        }
        ?>
            },
            limit: 10,
        });
    });
		</script>
			<br>
			<h5 align="center">or choose one</h5>
			<br>
			<ul class="collection">
		<?php
        foreach ($all as $induction) {
            ?>
			<li class="collection-item valign-wrapper" style="height: 35px;"><a
					href="./?doc=<?= $induction; ?>"><small><?= $induction; ?></small></a></li>		    
		    <?php
        }
        ?>
        </ul>
			<br>
			<?php if (in_array($_SESSION['user']->getClass(), Intranet::$GROUP_ADMINISTRATORS)) { ?>
			<p style="margin-top: -8px;">
				<b>Administrators - note</b>: induction questionnaires must be added
				manually as they use a special file format and layout. Please email
				the site maintainer with the questionnaire and training documents
				(PDFs, PowerPoints, etc) to begin the conversion process.
			</p>
				<?php } ?>
<?php
    } else {
        $doc = $_GET['doc'];
        
        $induction = InductionDocument::fromFile($doc);
        if (isset($_POST['submit'])) {
            if (count($_POST['induction']) != $induction->getQuestionsCount()) {
                @header('Location: ' . $_SERVER['REQUEST_URI'] . '&error');
                echo ('<meta http-equiv="refresh" content="0; url=./?error&doc=' . $doc . '">');
                die();
            } else {
                if (isset($_GET['error'])) {
                    unset($_GET['error']);
                }
            }
            
            $count = 0;
            for ($i = 0; $i < $induction->getQuestionsCount(); $i ++) {
                if ($_POST['induction'][$i] == $induction->getCorrectAnswer($i + 1))
                    $count ++;
            }
            
            $answers = implode(',', $_POST['induction']);
            $contact = isset($_POST['contact']) ? Intranet::getDbLink()->real_escape_string($_POST['contact']) : '';
            $id = GUID();
            
            Intranet::getDbLink()->query('INSERT INTO `inductions` (`id`, `recipient`, `contact`, `doc_number`, `date`, `validfor`, `answers`) VALUES (\'' . $id . '\', \'' . $_SESSION['user']->getId() . '\', \'' . $contact . '\', \'' . $doc . '\', CURRENT_TIMESTAMP, \'' . $induction->getValidFor() . '\', \'' . $answers . '\');');
            
            $result = InductionResult::fromDatabase($id);
            
            IntranetMail::InductionResultMail($result->getRecipient());
            
            if ($result->getPass()) {
                ?>
<br>
			<div class="card-panel green darken-2">
				<p class="white-text">Congratulations, <?= $_SESSION['user']->getNameFirst(); ?>! You have passed this induction. Please visit <a
						href="./../View/?id=<?= $result->getId(); ?>&print"
						style="color: #6a1b9a">this link</a> and print out your induction
					certificate. You will also be emailed this link. <b>If you do not
						receive an email, please contact an administrator!</b> <br> <br> <a
						class="waves-effect waves-light btn blue darken-3"
						href="<?= Config::getProperty('BaseDirWebserver'); ?>">Back home</a>
				</p>
			</div>
			<br>
<?php
                IntranetMail::InductionPassMail($result->getRecipient());
            } else {
                ?>
<br>
			<div class="card-panel red darken-2">
				<p class="white-text">You failed to pass this induction. You need a score of <?= $induction->getCorrectRequired(); ?>/<?= $induction->getQuestionsCount(); ?> (your score: <?= $result->getCorrect(); ?>). Have you read all required training?</p>
			</div>
			<br>
<?php
            }
        }
        ?>
<h4 align="center"><?= $induction->getTitle(); ?></h4>
<?php
        if (isset($_GET['error'])) {
            ?>
<p align="center">
				<font color="red">Error! Did you answer <b>all <?= $induction->getQuestionsCount(); ?> questions</b>?
					Please try again.
				</font>
			</p>
<?php
        }
        ?>
<script type="text/javascript">
		$(document).ready(function(){
			$("#continue").click(function(){
				$("#preamble").fadeOut();
		        $("#content").fadeIn();
		        window.open("./../View_Training/?doc=<?= $induction->getNumber(); ?>", '_blank');
		    });
		});
</script>
			<form action="./?doc=<?= $doc ?>" method="POST">
				<br style="display: block; margin: 10px 0;">
				<div id="preamble" align="center">
					<p style="text-align: center;">When you press continue, the
						training for this induction will download; you must read it before
						you make this attempt. If it does not download, a link will be
						provided above the questions.</p>
					<br> <a id="continue"
						class="waves-effect waves-light btn blue darken-3">Continue</a> <br>
					<br>
				</div>
				<div id="content" hidden>
					<p style="text-align: center;">
						<a href="./../View_Training/?doc=<?= $doc ?>" target="_blank"><i>Please
								make sure you have read all training documents before answering
								the questions below (click here to open the training again).</i></a>
					</p>
					<div class="input-field inline" style="width: 200px;">
						<input id="contact" name="contact" type="text"> <label
							for="contact">Armour Contact (optional)</label>
					</div>
			<?php
        for ($i = 0; $i < count($induction->getQuestions()); $i ++) {
            ?>
			<h5>Question <?= ($i + 1); ?></h5>
					<p>
						<b><?= $induction->getQuestions () [$i]; ?></b>
					</p>
			<?php
            for ($j = 0; $j < count($induction->getAnswers()[$i]); $j ++) {
                ?>
				<p>
					<?php
                $incorrect = (InductionDocument::letterToNum($induction->getCorrectAnswer($i + 1)) !== array_keys($induction->getAnswers()[$i])[$j] + 1);
                if ($incorrect) {
                    echo '<a class="tooltipped" data-position="right" data-delay="50" data-tooltip="Are you sure this is the right answer?">';
                }
                ?>
					<input type="radio" name="induction[<?= $i ?>]"
							value="<?= InductionDocument::numToLetter($j + 1); ?>"
							id="<?= $i . InductionDocument::numToLetter($j + 1); ?>"
							style="margin-bottom: 0px;" /> <label
							for="<?= $i . InductionDocument::numToLetter($j + 1); ?>"><?= $induction->getAnswers()[$i][$j]; ?></label>
						<?php if ($incorrect) echo '</a>'; ?>
				</p>
					<br>
			<?php
            }
        }
        ?>
				<button style="display: inline-block;"
						class="btn waves-effect waves-light cyan lighten-2" type="submit"
						name="submit">Submit</button>
				</div>
			</form>
<?php
    }
    ?>
	</div>
	</div>
	<br>
	<br>
	<br>
	</main>
<?php
    require_once (Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'style' . DIRECTORY_SEPARATOR . 'footer.php');
}
