<?php
require_once (__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'config.php');

if (in_array($_SESSION['user']->getClass(), Intranet::$GROUP_ADMINISTRATORS)) {
    require_once (Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'style' . DIRECTORY_SEPARATOR . 'header.php');
    ?>
<body class="light-blue darken-4">
	<main class="light-blue darken-3"> <br>
	<div class="container">
		<div class="card-panel">
			<h4 align="center">Manage Induction Results</h4>
			<br>
	<?php
    $result = Intranet::getDbLink()->query('SELECT * FROM `inductions` ORDER BY `date` DESC;');
    $inductions = $result->fetch_all(MYSQLI_ASSOC);
    ?>
			<h5>All Inductions</h5>
			<br>
			<table>
				<thead>
					<tr>
						<th data-field="recipient">Recipient (Click to Inspect)</th>
						<th data-field="name">Document Title (Hover for ID)</th>
						<th data-field="contact">Contact</th>
						<th data-field="date">Date</th>
						<th data-field="valid-until">Valid Until</th>
						<th data-field="view"></th>
						<th data-field="delete"></th>
					</tr>
				</thead>
				<tbody>
				
		<?php
    if (empty($inductions)) {
        ?>
					<tr>
						<th colspan="9" style="text-align: center;">There are currently no
							induction results.</th>
					</tr>
		<?php
    }
    
    foreach ($inductions as $induction) {
        $result = InductionResult::fromDatabase($induction['id']);
        $user = User::fromDatabase($result->getRecipient());
        $quiz = InductionDocument::fromFile($result->getDocNumber());
        ?>
					<tr>
						<td><small><a
								href="<?= Config::getProperty('BaseDirWebserver'); ?>/Users/View/?id=<?= $user->getId(); ?>"><?= $user->getNameFull(); ?></a></small></td>
						<td><small><a class="tooltipped" data-position="top"
								data-delay="50" data-tooltip="<?= $result->getDocNumber(); ?>"><?= $quiz->getTitle(); ?></a></small></td>
						<td><small><?= $result->getContact() == "" ? 'N/A' : $result->getContact(); ?></small></td>
						<td><small><?= date('j F Y', strtotime($result->getDate())); ?></small></td>
						<td><small><?= $result->getValidUntil(); ?></small></td>
						<td style="text-align: center;"><a
							href="./../View/?id=<?= $result->getId(); ?>"
							class="valign-wrapper"> <!-- <small>view</small> --> <i
								class="material-icons tiny">visibility</i>
						</a></td>
						<td style="text-align: center;"><a
							href="./../Delete/?id=<?= $result->getId(); ?>"> <!-- <small>delete</small> -->
								<i class="material-icons tiny">delete</i></a></td>
					</tr>			
	<?php
    }
    ?>
				</tbody>
			</table>
			<p align="center">
				<a class="btn waves-effect waves-light cyan lighten-2"
					href="./../Complete">Complete Induction/Generate Link</a> &nbsp; <a
					class="btn waves-effect waves-light light-blue darken-3"
					href="<?= (isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : Config::getProperty('BaseDirWebserver'); ?>">Back</a>
			</p>
		</div>
	</div>
	</main>
	<?php
    require_once (Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'style' . DIRECTORY_SEPARATOR . 'footer.php');
} else {
    header('Location: ' . Config::getProperty('BaseDirWebserver'));
}
