<?php
require_once (__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'config.php');
if (in_array($_SESSION['user']->getClass(), Intranet::$GROUP_CONTRIBUTORS)) {
    if (! isset($_GET['id'])) {
        header('Location: ./../Manage');
    }
    
    $id = $_GET['id'];
    
    require_once (Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'style' . DIRECTORY_SEPARATOR . 'header.php');
    ?>
<body class="light-blue darken-4">
	<main class="light-blue darken-3"> <br>
	<div class="container">
		<div class="card-panel">
			<h4 align="center">Manage Inductions</h4>
			<br> 
	<?php
    if (isset($_GET['confirm'])) {
        Intranet::getDbLink()->query('DELETE FROM `inductions` WHERE `id`=\'' . $id . '\';');
        @header('Location: ./../Manage');
        echo ('<meta http-equiv="refresh" content="0; url=./../Manage">');
        die();
    }
    
    $record = InductionResult::fromDatabase($id);
    $user = User::fromDatabase($record->getRecipient());
    $quiz = InductionDocument::fromFile($record->getDocNumber());
    ?>
<h5>Delete Record</h5>
			<br> <br>
			<!-- delet this -->
			<p align="center">
				<b>Are you sure you want to delete this record?</b> <br>
			</p>

			<table>
				<tbody>
					<tr>
						<th>Name</th>
						<td><?= $quiz->getTitle(); ?></td>
					</tr>
					<tr>
						<th>Recipient</th>
						<td><a
							href="<?= Config::getProperty(); ?>/Users/View/?id=<?= $user->getId(); ?>"><?= $user->getNameFull(); ?></a></td>
					</tr>
					<tr>
						<th>Date</th>
						<td><?= date('j F Y', strtotime($record->getDate())) . ' &mdash; ' . $record->getValidUntil(); ?></td>
					</tr>
				</tbody>
			</table>
			<br>
			<p align="center">
				<a class="waves-effect waves-light btn green accent-4"
					href="<?= isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : './../Manage' ?>">No</a>
				&nbsp; <a class="waves-effect waves-light btn amber darken-3"
					href="./?id=<?= $record->getId(); ?>&confirm">Yes</a>
			</p>
		</div>
	</div>
	</main>
<?php
    require_once (Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'style' . DIRECTORY_SEPARATOR . 'footer.php');
}
