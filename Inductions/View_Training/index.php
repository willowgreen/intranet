<?php
require_once (__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'config.php');
{
    // download the pptx file needed for training prior to an induction
    
    $doc = $_GET['doc'];
    // $file = Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'Inductions' . DIRECTORY_SEPARATOR . 'Files' . DIRECTORY_SEPARATOR . $doc_number . '.pptx';
    $file = Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'Inductions' . DIRECTORY_SEPARATOR . 'Files' . DIRECTORY_SEPARATOR . $doc . '.pdf';
    $document = Document::fromDatabase($doc);
    
    header('Content-description: File Transfer');
    header('Content-type: ' . mime_content_type($file));
    header('Content-disposition: inline; filename="' . basename($file) . '"'); // 'inline' refers to showing the attachment in-browser if possible
    header('Expires: 0');
    header('Cache-control: must-revalidate');
    header('Pragma: public');
    header('Content-length: ' . filesize($file));
    readfile($file); // output the file's contents to the client
    
    header('Location: ./?doc=' . $doc);
}
