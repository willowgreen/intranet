<?php
require_once (__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'config.php');

class User
{

    const PICTURE_BLANK = 10;

    const PICTURE_URL = 20;

    const PICTURE_FILE = 30;

    private $id;

    private $class;

    private $email;

    private $password;

    private $name_first;

    private $name_last;

    private $suspended;

    public function __construct($id, $class, $email, $password, $name_first, $name_last, $emergency_name, $emergency_relationship, $emergency_number, $suspended)
    {
        $this->setId($id);
        $this->setClass($class);
        $this->setEmail($email);
        $this->setPassword($password);
        $this->setNameFirst($name_first);
        $this->setNameLast($name_last);
        $this->setEmergencyContact_Name($emergency_name);
        $this->setEmergencyContact_Relationship($emergency_relationship);
        $this->setEmergencyContact_Number($emergency_number);
        $this->setSuspended((bool) $suspended);
    }

    public static function fromDatabase($id)
    {
        $result = Intranet::getDbLink()->query('SELECT * FROM `users` WHERE `id`=\'' . Intranet::getDbLink()->real_escape_string($id) . '\';');
        $user = $result->fetch_assoc();
        return new self($user['id'], $user['class'], $user['email'], $user['password'], $user['name_first'], $user['name_last'], $user['emergency_name'], $user['emergency_relationship'], $user['emergency_number'], $user['suspended']);
    }

    public function getId()
    {
        return $this->id;
    }

    private function setId($id)
    {
        $this->id = $id;
    }

    public function getClass()
    {
        return $this->class;
    }

    private function setClass($class)
    {
        $this->class = $class;
    }

    public function getEmail()
    {
        return $this->email;
    }

    private function setEmail($email)
    {
        $this->email = $email;
    }

    public function getPassword()
    {
        return $this->password;
    }

    private function setPassword($password)
    {
        $this->password = $password;
    }

    public function getNameFirst()
    {
        return $this->name_first;
    }

    private function setNameFirst($name_first)
    {
        $this->name_first = $name_first;
    }

    public function getNameLast()
    {
        return $this->name_last;
    }

    private function setNameLast($name_last)
    {
        $this->name_last = $name_last;
    }

    public function getNameFull()
    {
        return @$this->getNameFirst() . ' ' . @$this->getNameLast();
    }

    public function getSuspended()
    {
        return $this->suspended;
    }

    private function setSuspended($suspended)
    {
        $this->suspended = $suspended;
    }

    public function getEmergencyContact_Name()
    {
        return $this->emergency_name;
    }

    private function setEmergencyContact_Name($emergency_name)
    {
        $this->emergency_name = $emergency_name;
    }

    public function getEmergencyContact_Relationship()
    {
        return $this->emergency_relationship;
    }

    private function setEmergencyContact_Relationship($emergency_relationship)
    {
        $this->emergency_relationship = $emergency_relationship;
    }

    public function getEmergencyContact_Number()
    {
        return $this->emergency_number;
    }

    private function setEmergencyContact_Number($emergency_number)
    {
        return $this->emergency_number = $emergency_number;
    }

    public static function getAdministrators()
    {
        $administrators = Intranet::getDbLink()->query('SELECT `id` FROM `users` WHERE `class`=\'administrator\' OR `class`=\'developer\';');
        $administrators = $administrators->fetch_all();
        foreach ($administrators as $administrator) {
            $list[] = self::fromDatabase($administrator[0]);
        }
        return $list;
    }

    private function getPictureFilename()
    {
        return @Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'Users' . DIRECTORY_SEPARATOR . 'Pictures' . DIRECTORY_SEPARATOR . $this->getId();
    }

    public function getPictureType()
    {
        if (file_exists($this->getPictureFilename())) {
            if (strpos('http', fgets(fopen($this->getPictureFilename(), 'r'))) !== FALSE) {
                return self::PICTURE_URL;
            } else if (getimagesize($this->getPictureFilename())[0] == 0) {
                return self::PICTURE_BLANK;
            } else {
                return self::PICTURE_FILE;
            }
        }
        return self::PICTURE_BLANK;
    }

    public function getPictureImgContent()
    {
        switch ($this->getPictureType()) {
            default:
            case self::PICTURE_BLANK:
                return 'data:img/png;base64,' . preg_replace("/\r|\n/", "", @Config::getProperty('PictureBlankFile'));
                break;
            
            case self::PICTURE_URL:
                return htmlspecialchars(trim(file_get_contents($this->getPictureFilename())));
                break;
            
            case self::PICTURE_FILE:
                return 'data:img/png;base64,' . base64_encode(fread(fopen($this->getPictureFilename(), 'r'), filesize($this->getPictureFilename())));
                break;
        }
    }
}
