<?php

class TrainingLicence
{

    private $id;

    private $recipient;

    private $name;

    private $description;

    private $date;

    private $validfor;

    private $file_name;

    private $file_mimetype;

    private $notif_w_sent;

    private $notif_e_sent;

    public function __construct($id, $recipient, $name, $description, $date, $validfor, $file_name, $file_mimetype, $notif_w_sent, $notif_e_sent)
    {
        $this->setId($id);
        $this->setRecipient($recipient);
        $this->setName($name);
        $this->setDescription($description);
        $this->setDate($date);
        $this->setValidFor($validfor);
        $this->setFileName($file_name);
        $this->setFileMimeType($file_mimetype);
        $this->setNotifWarningSent($notif_w_sent);
        $this->setNotifExpirySent($notif_e_sent);
    }

    public static function fromDatabase($id)
    {
        $result = Intranet::getDbLink()->query('SELECT * FROM `training_licences` WHERE `id`=\'' . Intranet::getDbLink()->real_escape_string($id) . '\';');
        $record = $result->fetch_assoc();
        return new self($record['id'], $record['recipient'], $record['name'], $record['description'], $record['date'], $record['validfor'], $record['file_name'], $record['file_mimetype'], $record['notif_w_sent'], $record['notif_e_sent']);
    }

    public function getId()
    {
        return $this->id;
    }

    public function setId($id)
    {
        $this->id = $id;
    }

    public function getRecipient()
    {
        return $this->recipient;
    }

    public function setRecipient($recipient)
    {
        $this->recipient = $recipient;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($description)
    {
        $this->description = $description;
    }

    public function getDate($format = 'j F Y')
    {
        return date($format, strtotime($this->date));
    }

    public function setDate($date)
    {
        $this->date = $date;
    }

    public function getValidFor()
    {
        return $this->validfor;
    }

    public function setValidFor($validfor)
    {
        $this->validfor = $validfor;
    }

    public function getFileName()
    {
        return $this->file_name;
    }

    public function setFileName($file_name)
    {
        $this->file_name = $file_name;
    }

    public function getFileMimeType()
    {
        return $this->file_mimetype;
    }

    public function setFileMimeType($file_mimetype)
    {
        $this->file_mimetype = $file_mimetype;
    }

    public function getNotifWarningSent()
    {
        return (bool) $this->notif_w_sent;
    }

    public function setNotifWarningSent($notif_w_sent)
    {
        $this->notif_w_sent = $notif_w_sent;
    }

    public function getNotifExpirySent()
    {
        return (bool) $this->notif_e_sent;
    }

    public function setNotifExpirySent($notif_e_sent)
    {
        $this->notif_e_sent = $notif_e_sent;
    }

    public function getExpired()
    {
        return $this->getValidFor() == 0 ? false : (time() >= strtotime('+' . $this->getValidFor() . ' years', strtotime($this->getDate())));
    }

    public function getWarning()
    {
        if ($this->getValidFor() == 0)
            return false;
        
        $today = new DateTime('today');
        $diff = $today->diff(new DateTime($this->getValidUntil()));
        return $diff->days <= Config::getProperty('TrainingLicenceWarningGap');
    }

    public function getValidUntil()
    {
        return date('j F Y', strtotime('+' . $this->getValidFor() . ' years', strtotime($this->getDate())));
    }

    public function getFile()
    {
        return Config::getProperty('BaseDir') . 'Training_Licences' . DIRECTORY_SEPARATOR . 'Files' . DIRECTORY_SEPARATOR . $this->getId();
    }

    public function getFileExists()
    {
        return file_exists(Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'Training_Licences' . DIRECTORY_SEPARATOR . 'Files' . DIRECTORY_SEPARATOR . $this->getId());
    }
}
