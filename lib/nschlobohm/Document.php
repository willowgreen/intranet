<?php
class Document {
	private $id;
	private $filename;
	private $mimetype;
	private $class;
	private $name;
	private $description;
	private $date;
	private $uploader;
	public function __construct($id, $filename, $mimetype, $class, $name, $description, $date, $uploader) {
		$this->setId ( $id );
		$this->setFilename ( $filename );
		$this->setMimeType ( $mimetype );
		$this->setClass ( $class );
		$this->setName ( $name );
		$this->setDescription ( $description );
		$this->setDate ( $date );
		$this->setUploader ( $uploader );
	}
	public static function fromDatabase($id) {
		$result = Intranet::getDbLink ()->query ( 'SELECT * FROM `documents` WHERE `id`=\'' . $id . '\';' );
		$document = $result->fetch_assoc ();
		return new self ( $document ['id'], $document ['filename'], $document ['mimetype'], $document ['class'], $document ['name'], $document ['description'], $document ['date'], $document ['uploader'] );
	}
	public function getId() {
		return $this->id;
	}
	private function setId($id) {
		$this->id = $id;
	}
	public function getFilename() {
		return $this->filename;
	}
	private function setFilename($filename) {
		$this->filename = $filename;
	}
	public function getMimeType() {
		return $this->mimetype;
	}
	private function setMimeType($mimetype) {
		$this->mimetype = $mimetype;
	}
	public function getClass() {
		return $this->class;
	}
	private function setClass($class) {
		$this->class = $class;
	}
	public function getName() {
		return $this->name;
	}
	private function setName($name) {
		$this->name = $name;
	}
	public function getDescription() {
		return $this->description;
	}
	private function setDescription($description) {
		$this->description = $description;
	}
	public function getDate() {
		return $this->date;
	}
	private function setDate($date) {
		$this->date = $date;
	}
	public function getUploader() {
		return $this->uploader;
	}
	private function setUploader($uploader) {
		$this->uploader = $uploader;
	}
	public static function getAllDocumentFilenames() {
		return array_diff ( scandir ( Config::getProperty ( 'DocumentsDir' ) ), array (
				'..',
				'.' 
		) );
	}
}
