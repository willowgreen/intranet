<?php

class IntranetMail
{

    private function getPHPMailerObject()
    {
        $email = new PHPMailer();
        $email->isSMTP();
        $email->Host = 'smtp.gmail.com';
        $email->SMTPDebug = false;
        $email->Port = 587;
        $email->SMTPSecure = 'tls';
        $email->SMTPAuth = true;
        $email->Username = Config::getProperty('MailAddress');
        $email->Password = Config::getProperty('MailPassword');
        $email->setFrom(Config::getProperty('MailAddress'), Config::getProperty('SiteOwner'));
        $email->addReplyTo(Config::getProperty('MailAddress'), Config::getProperty('SiteOwner'));
        
        return $email;
    }

    private function getURL()
    {
        $url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        return substr($url, 0, strrpos($url, '/') + 1);
    }

    public function sendWelcome($id)
    {
        $user = User::fromDatabase($id);
        
        $to = $user->getNameFull() . '<' . $user->getEmail() . '>';
        $subject = 'Welcome to ' . Config::getProperty('SiteTitle');
        $url = 'http://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
        $url = substr($url, 0, strrpos($url, '/') + 1);
        
        $message = $user->getNameFirst() . '<br><br>';
        $message .= 'Welcome to ' . Config::getProperty('SiteTitle') . '. ';
        $message .= 'You now have an account (classed as a(n) ' . $user->getClass() . ') and you can log in at ' . $url . ' to start using the system right now.<br><br>';
        $message .= '<hr><b>' . Config::getProperty('SiteOwner') . '</b>';
        
        mail($to, $subject, $message, self::getHeaders());
    }

    public function sendMessage($id, $subject, $message)
    {
        $user = User::fromDatabase($id);
        $to = $user->getNameFull() . '<' . $user->getEmail() . '>';
        mail($to, $subject, $message);
    }

    public static function TrainingLicenceWarningEmail($id)
    {
        $record = TrainingLicence::fromDatabase($id);
        $user = User::fromDatabase($record->getRecipient());
        
        $to = self::getMailingList_Administrators();
        $subject = $user->getNameFull() . '\'s training/licence expires soon!';
        $message = 'Administrators<br><br>';
        $message .= 'The training/licence record entitled "' . $record->getName() . '" issued to ' . $user->getNameFull() . ' is due to expire on ' . $record->getValidUntil() . '.<br>';
        $message .= 'The record has been effective from ' . $record->getDate() . ', lasting for ' . $record->getValidFor() . ' year(s).<br><br>';
        $message .= '<hr><b>' . Config::getProperty('SiteOwner') . '</b>';
        
        mail($to, $subject, $message, self::getHeaders());
    }

    public function TrainingLicenceExpiryEmail($id)
    {
        $record = TrainingLicence::fromDatabase($id);
        $user = User::fromDatabase($record->getRecipient());
        $signoff = '<img alt="' . Config::getProperty('SiteOwner') . '" src="data:image/png;base64,' . Intranet::getLogo_base64() . '" style="width: 100%; max-width: 130px;">';
        
        $email = self::getPHPMailerObject();
        
        foreach (User::getAdministrators() as $administrator) {
            $email->addAddress($administrator->getEmail(), $administrator->getNameFull());
        }
        
        $email->Subject = ($user->getNameFull() . '\'s training/licence');
        
        $subject = $user->getNameFull() . '\'s training/licence has expired!';
        $email->Body = <<<EOT
        Administrators<br><br>
        <hr>$signoff
EOT;
        $email->Body = 'Administrators<br><br>';
        $email->Body .= 'The training/licence record entitled "' . $record->getName() . '" issued to ' . $user->getNameFull() . ' has expired.<br>';
        $email->Body .= 'The record had been effective from ' . $record->getDate() . ', lasting for ' . $record->getValidFor() . ' year(s).<br><br>';
        $email->Body .= '<hr><img alt="' . Config::getProperty('SiteOwner') . '" src="data:image/png;base64,' . Intranet::getLogo_base64() . '" style="width: 100%; max-width: 130px;">';
        
        $email->isHTML(true);
        if (! $email->send()) {
            echo 'Email could not be sent to some or all of the administration team. Please contact your system administrator. Mailer error: ' . $email->ErrorInfo;
            die();
        }
    }

    public function InductionResultMail($id)
    {
        $record = Intranet::getDbLink()->query('SELECT `id` FROM `inductions` WHERE `recipient`=\'' . $id . '\' ORDER BY `date` DESC LIMIT 1;');
        $record = $record->fetch_all()[0];
        $result = InductionResult::fromDatabase($record[0]);
        $quiz = InductionDocument::fromFile($result->getDocNumber());
        $user = User::fromDatabase($result->getRecipient());
        
        $email = self::getPHPMailerObject();
        
        foreach (User::getAdministrators() as $administrator) {
            $email->addAddress($administrator->getEmail(), $administrator->getNameFull());
        }
        
        $email->Subject = ($user->getNameFull() . '\'s induction result');
        
        // Body vars
        $user_email = $user->getEmail();
        $user_fullname = $user->getNameFull();
        $quiz_title = $quiz->getTitle();
        $quiz_questions_number = $quiz->getQuestionsCount();
        $quiz_questions_correctneeded = $quiz->getCorrectRequired();
        $result_correct_number = $result->getCorrect();
        $result_date = $result->getDate();
        $result_status = $result->getPass() ? 'Pass' : 'Fail';
        $signoff = '<img alt="' . Config::getProperty('SiteOwner') . '" src="data:image/png;base64,' . Intranet::getLogo_base64() . '" style="width: 100%; max-width: 130px;">';
        
        $email->Body = <<<EOT
		Administrators<br><br>An induction result has just been posted for $user_fullname ($user_email).<br>
		<table border="1" cellpadding="5" cellspacing="5">
		<tr><th>Completed:</th><td>$result_date</td></tr>
		<tr><th>Pass/Fail:</th><td>$result_status ($result_correct_number/$quiz_questions_number,  $quiz_questions_correctneeded required)</td></tr>
		</table><br><br>
		<hr>$signoff
EOT;
        
        $email->isHTML(true);
        if (! $email->send()) {
            echo 'Although your result has been added to the database, an email could not be sent to you and intranet administration team. Please contact the administrator who can provide you with the link to your induction. Mailer error: ' . $email->ErrorInfo;
            die();
        }
    }

    public function InductionPassMail($id)
    {
        $record = Intranet::getDbLink()->query('SELECT `id` FROM `inductions` WHERE `recipient`=\'' . $id . '\' ORDER BY `date` DESC LIMIT 1;');
        $record = $record->fetch_all()[0];
        
        $result = InductionResult::fromDatabase($record[0]);
        $quiz = InductionDocument::fromFile($result->getDocNumber());
        $user = User::fromDatabase($result->getRecipient());
        
        $email = self::getPHPMailerObject();
        
        $email->addAddress($user->getEmail(), $user->getNameFull());
        
        $email->Subject = 'Your induction result';
        $email->Body = $user->getNameFirst() . '<br><br>';
        $email->Body .= 'You have passed your <i>' . $quiz->getTitle() . '</i> induction.<br>';
        
        $email->Body .= 'Please <a href="' . self::getURL() . '/Inductions/View/?id=' . $result->getId() . '&print">click here</a> and print out your induction certificate.';
        $email->Body .= '<br><br>';
        $email->Body .= '<hr><b>' . Config::getProperty('SiteOwner') . '</b>';
        
        $email->isHTML(true);
        if (! $email->send()) {
            echo 'Email could not be sent. Please contact system administrator. Mailer error: ' . $email->ErrorInfo;
            die();
        }
    }

    public function InductionWarningMail($id)
    {
        $record = Intranet::getDbLink()->query('SELECT * FROM `inductions` WHERE `id`=\'' . $id . '\' ORDER BY `date` DESC LIMIT 1;');
        $record = $record->fetch_assoc();
        
        $user = User::fromDatabase($record['recipient']);
        $result = InductionResult::fromDatabase($record['id']);
        $quiz = InductionDocument::fromFile($record['doc_number']);
        
        // $this->getHeaders();
        
        $to = self::getMailingList_Administrators() . ', ' . $user->getNameFull() . ' <' . $user->getEmail() . '>';
        $email->subject = $user->getNameFull() . '\'s induction result expiry warning';
        
        $message = $user->getNameFull() . '<br><br>';
        $message .= 'Your induction entitled "' . $quiz->getTitle() . '" is <b>due to expire on ' . $result->getValidUntil() . '</b>.<br>';
        $message .= 'If this licence needs to be renewed, please do so soon. The company\'s administrators have also been contacted regarding this.';
        $message .= '<br><br>';
        
        // ';';';';'
        
        $message .= '<hr><b>' . Config::getProperty('SiteOwner') . '</b>';
        
        mail($to, $subject, $message, self::getHeaders());
    }

    public function InductionExpiryMail($id)
    {
        $record = Intranet::getDbLink()->query('SELECT * FROM `inductions` WHERE `id`=\'' . $id . '\' ORDER BY `date` DESC LIMIT 1;');
        $record = $record->fetch_assoc();
        
        $user = User::fromDatabase($record['recipient']);
        $result = InductionResult::fromDatabase($record['id']);
        $quiz = InductionDocument::fromFile($record['doc_number']);
        
        $to = self::getMailingList_Administrators() . ', ' . $user->getNameFull() . ' <' . $user->getEmail() . '>';
        $subject = $user->getNameFull() . '\'s induction result expiry warning';
        
        $message = $user->getNameFull() . '<br><br>';
        $message .= 'Your induction entitled "' . $quiz->getTitle() . '" <b>has expired</b>.<br>';
        $message .= 'If this licence needs to be renewed, please do so immediately. The company\'s administrators have also been contacted regarding this.';
        $message .= '<br><br>';
        
        $message .= '<hr><b>' . Config::getProperty('SiteOwner') . '</b>';
        
        mail($to, $subject, $message, self::getHeaders());
    }
}