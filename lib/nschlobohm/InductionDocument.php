<?php

class InductionDocument
{

    private $title;

    private $number;

    private $questions;

    private $answers;

    private $correct_answers;

    private $correct_requrired;

    public function __construct($title, $number, $questions, $answers, $correct_answers, $correct_required, $validfor)
    {
        $this->setTitle($title);
        $this->setNumber($number);
        $this->setQuestions($questions);
        $this->setAnswers($answers);
        $this->setCorrectAnswers($correct_answers);
        $this->setCorrectRequired($correct_required);
        $this->setQuestionsCount(count($this->getQuestions()));
        $this->setValidFor($validfor);
    }

    public static function fromFile($number)
    {
        $document = json_decode(file_get_contents(Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'Inductions' . DIRECTORY_SEPARATOR . 'Files' . DIRECTORY_SEPARATOR . $number), true);
        
        $q = $document['questions'];
        $questions = array();
        $answers = array();
        
        foreach ($document['questions'] as $qnum => $question) {
            array_push($questions, key($question));
            $ans = array();
            $question_answers = reset($question);
            foreach ($question_answers as $answer) {
                array_push($ans, $answer);
            }
            array_push($answers, $ans);
        }
        
        return new self($document['meta']['title'], $number, $questions, $answers, $document['correct'], $document['meta']['minimum_correct'], $document['meta']['valid_for']);
    }

    public function getTitle()
    {
        return $this->title;
    }

    private function setTitle($title)
    {
        $this->title = $title;
    }

    public function getNumber()
    {
        return $this->number;
    }

    private function setNumber($number)
    {
        $this->number = $number;
    }

    public function getQuestions()
    {
        return $this->questions;
    }

    private function setQuestions($questions)
    {
        $this->questions = $questions;
    }

    public function getAnswers()
    {
        return $this->answers;
    }

    private function setAnswers($answers)
    {
        $this->answers = $answers;
    }

    public function getCorrectAnswer($question)
    {
        return $this->getCorrectAnswers()[$question --];
    }

    public function getCorrectAnswers()
    {
        return $this->correct_answers;
    }

    private function setCorrectAnswers($correct_answers)
    {
        $this->correct_answers = $correct_answers;
    }

    public function getCorrectRequired()
    {
        return $this->correct_requrired;
    }

    private function setCorrectRequired($correct_required)
    {
        $this->correct_requrired = $correct_required;
    }

    public function getValidFor()
    {
        return $this->validfor;
    }

    private function setValidFor($validfor)
    {
        $this->validfor = $validfor;
    }

    public function numToLetter($num)
    {
        switch ($num) {
            case 1:
                return 'a';
            case 2:
                return 'b';
            case 3:
                return 'c';
            case 4:
                return 'd';
            case 5:
                return 'e';
            case 6:
                return 'f';
            case 7:
                return 'g';
            case 8:
                return 'h';
            case 9:
                return 'i';
            case 10:
                return 'j';
            case 11:
                return 'k';
            case 12:
                return 'l';
            case 13:
                return 'm';
            case 14:
                return 'n';
            case 15:
                return 'o';
            case 16:
                return 'p';
            case 17:
                return 'q';
            case 18:
                return 'r';
            case 19:
                return 's';
            case 20:
                return 't';
            case 21:
                return 'u';
            case 22:
                return 'v';
            case 23:
                return 'w';
            case 24:
                return 'x';
            case 25:
                return 'y';
            case 26:
                return 'z';
        }
    }

    public function letterToNum($letter)
    {
        switch ($letter) {
            case 'a':
                return 1;
            case 'b':
                return 2;
            case 'c':
                return 3;
            case 'd':
                return 4;
            case 'e':
                return 5;
            case 'f':
                return 6;
            case 'g':
                return 7;
            case 'h':
                return 8;
            case 'i':
                return 9;
            case 'j':
                return 10;
            case 'k':
                return 11;
            case 'l':
                return 12;
            case 'm':
                return 13;
            case 'n':
                return 14;
            case 'o':
                return 15;
            case 'p':
                return 16;
            case 'q':
                return 17;
            case 'r':
                return 18;
            case 's':
                return 19;
            case 't':
                return 20;
            case 'u':
                return 21;
            case 'v':
                return 22;
            case 'w':
                return 23;
            case 'x':
                return 24;
            case 'y':
                return 25;
            case 'z':
                return 26;
        }
    }

    public static function getAllFilenames()
    {
        foreach (array_values(array_diff(scandir(Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'Inductions' . DIRECTORY_SEPARATOR . 'Files'), array(
            '.',
            '..',
            '.DS_Store',
            '.htaccess'
        ))) as $file) {
            if (strpos($file, '.') === false)
                $filenames[] = $file;
        }
        
        return $filenames;
    }

    public function getQuestionsCount()
    {
        return $this->questions_count;
    }

    private function setQuestionsCount($questions_count)
    {
        $this->questions_count = $questions_count;
    }
}