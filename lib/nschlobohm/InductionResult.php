<?php
class InductionResult {
	private $id;
	private $recipient;
	private $contact;
	private $doc_number;
	private $date;
	private $validfor;
	private $answers;
	private $correct;
	public function __construct($id, $recipient, $contact, $doc_number, $date, $validfor, $answers) {
		$this->setId ( $id );
		$this->setRecipient ( $recipient );
		$this->setContact ( $contact );
		$this->setDocNumber ( $doc_number );
		$this->setDate ( $date );
		$this->setValidFor ( $validfor );
		$this->setAnswers ( explode ( ',', $answers ) );
	}
	public static function fromDatabase($id) {
		$result = Intranet::getDbLink ()->query ( 'SELECT * FROM `inductions` WHERE `id`=\'' . Intranet::getDbLink ()->real_escape_string ( $id ) . '\';' );
		$induction = $result->fetch_assoc ();
		return new self ( $induction ['id'], $induction ['recipient'], $induction ['contact'], $induction ['doc_number'], $induction ['date'], $induction ['validfor'], $induction ['answers'] );
	}
	public function getId() {
		return $this->id;
	}
	private function setId($id) {
		$this->id = $id;
	}
	public function getRecipient() {
		return $this->recipient;
	}
	private function setRecipient($recipient) {
		$this->recipient = $recipient;
	}
	public function getContact() {
		return $this->contact;
	}
	private function setContact($contact) {
		$this->contact = $contact;
	}
	public function getDocNumber() {
		return $this->doc_number;
	}
	private function setDocNumber($doc_number) {
		$this->doc_number = $doc_number;
	}
	public function getDate() {
		return $this->date;
	}
	private function setDate($date) {
		$this->date = $date;
	}
	public function getValidFor() {
		return $this->validfor;
	}
	private function setValidFor($validfor) {
		$this->validfor = $validfor;
	}
	public function getAnswers() {
		return $this->answers;
	}
	private function setAnswers($answers) {
		$this->answers = $answers;
	}
	public function getPass() {
		return $this->getCorrect () >= InductionDocument::fromFile ( $this->getDocNumber () )->getCorrectRequired ();
	}
	public function getCorrect() {
		$answers = $this->getAnswers ();
		$correct_answers = array_values ( InductionDocument::fromFile ( $this->getDocNumber () )->getCorrectAnswers () );
		
		$correct = 0;
		
		for($i = 0; $i < count ( $correct_answers ); $i ++) {
			if ($answers [$i] == $correct_answers [$i])
				$correct ++;
		}
		return $correct;
	}
	public function getValidUntil() {
		if (( int ) $this->getValidFor () == 0)
			return date ( 'j F Y', strtotime ( '+6 months', strtotime ( $this->getDate () ) ) );
		else
			return date ( 'j F Y', strtotime ( '+' . $this->getValidFor () . ' years', strtotime ( $this->getDate () ) ) );
	}
}