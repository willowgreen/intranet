<?php
define('INDEX', true);
require_once (__DIR__ . DIRECTORY_SEPARATOR . 'config.php');
if (in_array($_SESSION['user']->getClass(), Intranet::$GROUP_INDUCTEES) && ! (in_array($_SESSION['user']->getClass(), Intranet::$GROUP_CONTRIBUTORS))) {
    @header('Location: ' . Config::getProperty('BaseDirWebserver') . '/Inductions/Complete');
    die();
}

require_once (Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'style' . DIRECTORY_SEPARATOR . 'header.php');

?>
<body class="light-blue darken-4">
	<main class="light-blue darken-3"> <br>
	<div class="container">
		<?php
if (in_array($_SESSION['user']->getClass(), Intranet::$GROUP_CONTRIBUTORS)) {
    // documents are only available to employees (contributors) and higher
    ?>
    <div class="card-panel">
			<h4 align="center">
				<a
					href="<?= Config::getProperty('BaseDirWebserver'); ?>/Noticeboard/Edit"
					title="Edit the noticeboard">Noticeboard</a>
			</h4>
			<?php
    $result = Intranet::getDbLink()->query('SELECT * FROM `noticeboard` LIMIT 1;');
    $notices = $result->fetch_assoc();
    
    $updater = User::fromDatabase($notices['updater']);
    if (! empty($notices['message'])) {
        echo ('<blockquote>' . nl2br(htmlspecialchars($notices['message'])) . '</blockquote>');
        echo ('<div align="center"><small>Last updated ' . date('j F Y, g:i a', strtotime($notices['updated'])) . ' by ');
        echo ('<a href="' . Config::getProperty('BaseDirWebserver') . '/Users/View/?id=' . $updater->getId() . '">' . $updater->getNameFull() . '</a></small></div>');
    } else {
        echo ('<p align="center">There are no notices to show.</p>');
    }
    ?>
		</div>
		<br> <br>
		<div class="card-panel">
			<div style="text-align: center;">
				<h4 style="display: inline;">
					<a
						href="<?= Config::getProperty('BaseDirWebserver'); ?>/Documents/Manage"
						title="Manage all documents">Documents</a>
				</h4>
				<br style="display: block; margin: 2px 0;"> <small>Last 5 are
					displayed.</small><br style="display: block; margin: 5px 0;">
			</div>
			<p align="center">
				<a
					href="<?= Config::getProperty('BaseDirWebserver'); ?>/Documents/Search"
					title="Search all documents"><i class="material-icons small">search</i></a>
			</p>
			<?php
    $result = Intranet::getDbLink()->query('SELECT `id` FROM `documents` ORDER BY `date` DESC LIMIT 5;');
    $documents = $result == false ? array() : @$result->fetch_all();
    if (! empty($documents)) {
        ?>
			<table class="highlight">
				<thead>
					<tr>
						<th data-field="name"><small>Name (Click to View)</small></th>
						<th data-field="description"><small>Description</small></th>
						<th data-field="uploader"><small>Uploader (Click to Inspect)</small></th>
						<th data-field="date"><small>Uploaded</small></th>
						<th data-field="edit"></th>
						<th data-field="delete"></th>
					</tr>
				</thead>
				<tbody>
       			<?php
        foreach ($documents as $document) {
            $document = Document::fromDatabase($document[0]);
            $uploader = User::fromDatabase($document->getUploader());
            ?>
        			<tr>
						<td><small><a
								href="<?= Config::getProperty('BaseDirWebserver'); ?>/Documents/View/?id=<?= $document->getId() ?>"
								title="View" class="valign-wrapper"><b><?= $document->getName() ?></b></a></small></td>
						<td><small><?= $document->getDescription() ?></small></td>
						<td><a class="tooltipped" data-position="top" data-delay="50"
							data-tooltip="<?= $uploader->getEmail() ?>"
							href="<?= Config::getProperty('BaseDirWebserver'); ?>/Users/View/?id=<?= $uploader->getId() ?>"><small>
							<?= $uploader->getNameFull() ?></small></a></td>
						<td><small><?= date('j F Y, g:i a', strtotime($document->getDate())); ?></small></td>
						<td><a
							href="<?= Config::getProperty('BaseDirWebserver'); ?>/Documents/Edit/?id=<?= $document->getId() ?>"
							title="Edit this document"><i class="material-icons tiny">mode_edit</i></a></td>
						<td><a
							href="<?= Config::getProperty('BaseDirWebserver'); ?>/Documents/Delete/?id=<?= $document->getId() ?>"
							title="Delete this document"><i class="material-icons tiny">delete</i></a></td>

					</tr>
        		<?php
        }
        ?>
        		</tbody>
			</table>
    <?php
    } else {
        echo ('<p align="center">There are no documents to show.</p>');
    }
    ?>
		</div>
		<?php
}

if (in_array($_SESSION['user']->getClass(), Intranet::$GROUP_ADMINISTRATORS)) {
    // induction results are only for administrators to see
    ?>
		<br> <br>
		<div class="card-panel">
			<div style="text-align: center;">
				<h4 style="display: inline;">
					<a
						href="<?= Config::getProperty('BaseDirWebserver'); ?>/Inductions/Manage"
						title="Manage all inductions">Inductions</a>
				</h4>
				<br style="display: block; margin: 2px 0;"> <small>Last 5 are
					displayed.</small><br style="display: block; margin: 5px 0;">
			</div>
			<?php
    $result = Intranet::getDbLink()->query('SELECT `id` FROM `inductions` ORDER BY `date` DESC LIMIT 5;');
    $results = ! $result ? array() : @$result->fetch_all(MYSQLI_ASSOC);
    if (! empty($results)) {
        ?>
			<table class="highlight">
				<thead>
					<tr>
						<th data-field="recipient"><small>Recipient (Click to Inspect)</small></th>
						<th data-field="doc_number-title"><small>Induction (Hover for Doc
								Number)</small></th>
						<th data-field="contact"><small>Contact</small></th>
						<th data-field="date"><small>Date</small></th>
						<th data-field="validuntil"><small>Valid Until</small></th>
						<th data-field="pass-fail"><small>Pass/Fail</small></th>
						<th data-field="view"></th>
						<th data-field="delete"></th>
					</tr>
				</thead>
				<tbody>
				<?php
        foreach ($results as $id) {
            $result = InductionResult::fromDatabase($id['id']);
            $user = User::fromDatabase($result->getRecipient());
            $quiz = InductionDocument::fromFile($result->getDocNumber());
            echo ('<tr>');
            echo ('<td><small><a class="tooltipped" data-position="top"
								data-delay="50" data-tooltip="' . $user->getEmail() . '" href="' . Config::getProperty('BaseDirWebserver') . '/Users/View/?id=' . $user->getId() . '">' . $user->getNameFull() . '</a></small></td>');
            echo ('<td><small><a class="tooltipped" data-position="top"
								data-delay="50" data-tooltip="' . $result->getDocNumber() . '">' . $quiz->getTitle() . '</a></small></td>');
            echo ('<td><small>' . ($result->getContact() == null || $result->getContact() == '' ? 'N/A' : $result->getContact()) . '</small></td>');
            echo ('<td><small>' . date('j F Y', strtotime($result->getDate())) . '</small></td>');
            echo ('<td><small>' . $result->getValidUntil() . '</small></td>');
            echo ('<td><small>' . ($result->getPass() ? '<font color="green">Pass</font>' : '<font color="red">Fail</font>') . '</small></td>');
            echo ('<td><a href="' . Config::getProperty('BaseDirWebserver') . '/Inductions/View/?id=' . $result->getId() . '"><i class="material-icons tiny">visibility</i></a></td>');
            echo ('<td><a href="' . Config::getProperty('BaseDirWebserver') . '/Inductions/Delete/?id=' . $result->getId() . '"><i class="material-icons tiny">delete</i></a></td>');
            echo ('</tr>');
        }
        ?>
        		</tbody>
			</table>
    		<?php
    } else {
        echo ('<p align="center">There are no results to show.</p>');
    }
    ?>
		</div>
		<?php
}

if (in_array($_SESSION['user']->getClass(), Intranet::$GROUP_ADMINISTRATORS)) {
    // training and licences are only for administrators to see
    ?>
		<br> <br>
		<div class="card-panel">
			<div style="text-align: center;">
				<h4 style="display: inline;">
					<a
						href="<?= Config::getProperty('BaseDirWebserver'); ?>/Training_Licences/Manage"
						title="Manage all training records/licences">Training/Licences</a>
				</h4>
				<br style="display: block; margin: 2px 0;"> <small>Last 5 are
					displayed.</small><br style="display: block; margin: 5px 0;">
			</div>
				<?php
    $result = Intranet::getDbLink()->query('SELECT `id` FROM `training_licences` ORDER BY `date` DESC LIMIT 5;');
    $results = $result == false ? array() : @$result->fetch_all();
    if (! empty($results)) {
        ?>
			<table class="highlight">
				<thead>
					<tr>
						<th data-field="recipient"><small>Recipient (Click to Inspect)</small></th>
						<th data-field="name"><small>Name (Click to View)</small></th>
						<th data-field="date"><small>Effective Date</small></th>
						<th data-field="validfor"><small>Valid Until</small></th>
						<th data-field="edit"></th>
						<th data-field="delete"></th>
					</tr>
				</thead>
				<tbody>
        		<?php
        foreach ($results as $record) {
            $record = TrainingLicence::fromDatabase($record[0]);
            $user = User::fromDatabase($record->getRecipient());
            ?>
					<tr
						<?= $record->getExpired() ? ' style="color:#e53935"' : ($record->getWarning() ? ' style="color:#f57c00"' : '') ?>>
						<td><small><a class="tooltipped" data-position="top"
								data-delay="50" data-tooltip="<?= $user->getEmail() ?>"
								href="<?= Config::getProperty('BaseDirWebserver'); ?>/Users/View/?id=<?= $user->getId() ?>"><?= $user->getNameFull() ?></a></small></td>
						<td><small><a
								href="<?= Config::getProperty('BaseDirWebserver'); ?>/Training_Licences/View/?id=<?= $record->getId() ?>"><b><?= $record->getName() ?></b></a></small></td>
						<td><small><?= $record->getDate() ?></small></td>
						<td><small><?= $record->getValidUntil() ?></small></td>
						<td><a
							href="<?= Config::getProperty('BaseDirWebserver'); ?>/Training_Licences/Manage/?edit&id=<?= $record->getId() ?>"><i
								class="material-icons tiny">mode_edit</i></a></td>
						<td><a
							href="<?= Config::getProperty('BaseDirWebserver'); ?>/Training_Licences/Delete/?id=<?= $record->getId() ?>"><i
								class="material-icons tiny">delete</i></a></td>
					</tr>
				<?php
        }
        ?>
        		</tbody>
			</table>
    		<?php
    } else {
        echo ('<p align="center">There are no training or licence records to show.</p>');
    }
    ?>
		</div>
		<?php
}
?>
	</div>
	<br>
	</main>
<?php
require_once (Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'style' . DIRECTORY_SEPARATOR . 'footer.php');
