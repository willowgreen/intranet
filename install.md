apt-get update

apt-get -y install apache2
apt-get -y install mysql-server
apt-get -y install php5 php5-mysql php5-mysqlnd libapache2-mod-php5

apt-get -y install mailutils

cd /var/www/html
rm -rf ./index.html

apt-get -y install git
git clone https://bitbucket.org/nickyschlobs/intranet.git .

mysql -p
SET GLOBAL time_zone = '+10:00';
CREATE USER 'armour'@'localhost' IDENTIFIED BY 'password';
GRANT ALL PRIVILIGES ON intranet.* TO 'armour'@'localhost' WITH GRANT OPTION;
CREATE USER 'armour'@'%' IDENTIFIED BY 'password';
GRANT ALL PRIVILIGES ON intranet.* TO 'armour'@'%' WITH GRANT OPTION;

nano /etc/apache2/apache2.conf
ErrorDocument 403 /403.html
ErrorDocument 404 /404.html

nano /etc/php5/apache2/php.ini
date.timezone = "Australia/Brisbane"
