<?php
require_once (__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'config.php');
if (in_array($_SESSION['user']->getClass(), Intranet::$GROUP_CONTRIBUTORS)) {
    require_once (Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'style' . DIRECTORY_SEPARATOR . 'header.php');
    ?>
<body class="light-blue darken-4">
	<main class="light-blue darken-3"> <br>
	<div class="container">
		<div class="card-panel">
			<h4 align="center">Manage Training/Licences</h4>
			<?php
    if (isset($_GET['edit'])) {
        if (! isset($_GET['id'])) {
            @header('Location: ./');
            echo ('<meta http-equiv="refresh" content="0; url=./">');
            die();
        }
        
        $id = $_GET['id'];
        $traininglicence = TrainingLicence::fromDatabase(Intranet::getDbLink()->real_escape_string($id));
        $recipient = User::fromDatabase($traininglicence->getRecipient());
        
        if (isset($_GET['submit'])) {
            if (is_null($recipient)) {
                @header('Location: ./');
                echo ('<meta http-equiv="refresh" content="0; url=./">');
                die();
            }
            
            $name = Intranet::getDbLink()->real_escape_string($_POST['name']);
            $description = Intranet::getDbLink()->real_escape_string($_POST['description']);
            $date = Intranet::getDbLink()->real_escape_string(date('Y-m-d', strtotime($_POST['date'])));
            $validfor = Intranet::getDbLink()->real_escape_string($_POST['validfor']);
            $file_name = Intranet::getDbLink()->real_escape_string($_FILES['file']['name']);
            $file_mimetype = Intranet::getDbLink()->real_escape_string($_FILES['file']['type']);
            
            Intranet::getDbLink()->query('UPDATE `training_licences` SET `name`=\'' . $name . '\', `description`=\'' . $description . '\', `date`=\'' . $date . '\', `validfor`=\'' . $validfor . '\', `file_name`=\'' . $file_name . '\', `file_mimetype`=\'' . $file_mimetype . '\' WHERE `id`=\'' . $traininglicence->getId() . '\';');
            
            if ($file_mimetype != '') {
                $destination = Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'Training_Licences' . DIRECTORY_SEPARATOR . 'Files' . DIRECTORY_SEPARATOR . $id;
                if (file_exists($destination))
                    unlink($destination);
                move_uploaded_file($_FILES['file']['tmp_name'], $destination);
            }
            @header('Location: ./../View/?id=' . $traininglicence->getId());
            echo ('<meta http-equiv="refresh" content="0; url=./../View/?id=' . $traininglicence->getId() . '">');
            die();
        }
        ?>
			<h5>Edit <?= $recipient->getNameFull(); ?>'s Record</h5>
			<form action="./?id=<?= $traininglicence->getId(); ?>&edit&submit"
				method="POST" enctype="multipart/form-data">
				<div class="input-field">
					<input id="name" name="name" type="text"
						class="autocomplete-name validate"
						value="<?= $traininglicence->getName(); ?>"> <label class="active"
						for="name">Training/Licence Name</label>
				</div>
				<div class="input-field">
					<textarea id="description" name="description"
						class="materialize-textarea validate"><?= $traininglicence->getDescription(); ?></textarea>
					<label for="description">Training/Licence Description</label>
				</div>
				<div class="input-field">
					<input id="date" name="date" type="date" class="datepicker"
						value="<?= $traininglicence->getDate('Y-m-d'); ?>"> <label
						class="active" for="date">Date Effective (Click to Choose)</label>
				</div>
				<div class="file-field input-field">
					<div class="btn light-blue darken-3">
						<span>File (Optional)</span> <input type="file" id="file"
							name="file">
					</div>

					<div class="file-path-wrapper">
						<input class="file-path validate" type="text"
							value="<?= $traininglicence->getFileExists() ? $traininglicence->getFileName() : ''; ?>">
					</div>
				</div>
				<br>
				<div class="input-field">
					<select class="browser-default" id="validfor" name="validfor">
						<option value="1" disabled>Valid For (Years)</option>
						<option value="0"
							<?= $traininglicence->getValidFor() == 0 ? ' selected' : ''; ?>>Infinite</option>
						<option value="1"
							<?= $traininglicence->getValidFor() == 1 ? ' selected' : ''; ?>>1
							Year</option>
						<option value="2"
							<?= $traininglicence->getValidFor() == 2 ? ' selected' : ''; ?>>2
							Years</option>
						<option value="3"
							<?= $traininglicence->getValidFor() == 3 ? ' selected' : ''; ?>>3
							Years</option>
						<option value="4"
							<?= $traininglicence->getValidFor() == 4 ? ' selected' : ''; ?>>4
							Years</option>
						<option value="5"
							<?= $traininglicence->getValidFor() == 5 ? ' selected' : ''; ?>>5
							Years</option>
					</select>
				</div>
				<br> <br>
				<button class="btn waves-effect waves-light light-blue darken-3"
					type="submit" name="submit">Submit</button>
				&nbsp; <a class="btn waves-effect waves-light light-blue darken-3"
					href="<?= Config::getProperty('BaseDirWebserver'); ?>/Training_Licences/View/?id=<?= $traininglicence->getId(); ?>">Back</a> 
				
				<?php
        if ($traininglicence->getFileExists()) {
            ?>	
					&nbsp; <a class="btn waves-effect waves-light light-blue darken-3"
					href="<?= Config::getProperty('BaseDirWebserver'); ?>/Training_Licences/Delete/?id=<?= $traininglicence->getId(); ?>&file">Delete
					Current File</a> <?php
        }
        ?>
				<br>

			</form>
		</div>
	</div>
	</main>
	<script type="text/javascript">
	$(document).ready(function () {
		$('input.autocomplete-recipient').autocomplete({
			data: {
	<?php
        $result = Intranet::getDbLink()->query('SELECT DISTINCT `recipient` FROM `training_licences` ORDER BY `recipient` ASC;');
        $recipients = $result->fetch_all();
        for ($i = 0; $i < count($recipients); $i ++) {
            echo ('"' . $recipients[$i][0] . '" : null' . ($i == count($recipients) ? '' : ',') . PHP_EOL);
        }
        ?>
			}
		});

		$('input.autocomplete-name').autocomplete({
			data: {
<?php
        $result = Intranet::getDbLink()->query('SELECT DISTINCT `name` FROM `training_licences` ORDER BY `name` ASC;');
        $names = $result->fetch_all();
        for ($i = 0; $i < count($names); $i ++) {
            echo ('"' . $names[$i][0] . '" : null' . ($i == count($names) ? '' : ',') . PHP_EOL);
        }
        ?>
			}
		});
	});
	</script>
		<?php
    } else {
        $traininglicences = array();
        $result = Intranet::getDbLink()->query('SELECT DISTINCT `id` FROM `training_licences` ORDER BY `date` DESC, `recipient` ASC;');
        foreach ($result->fetch_all() as $record) {
            if (! empty($result)) {
                $traininglicences[] = TrainingLicence::fromDatabase($record[0]);
            }
        }
        ?>
	<h5>All Records</h5>
	<p>
		When a record turns <font color="#e53935">this colour</font>, it has
		expired. When a record turns <font color="#f57c00">this colour</font>, it is going to expire within <?= Config::getProperty('TrainingLicenceWarningGap') ?> days.</p>
	<table class="highlight">
		<thead>
			<tr>
				<th data-field="name"><small>Name <small>(Click to View Record)</small></small></th>
				<th data-field="recipient"><small>Recipient <small>(Click to
							Inspect)</small></small></th>
				<th data-field="date"><small>Date Effective</small></th>
				<th data-field="validfor"><small>Valid Until</small></th>
				<th data-field="edit" width="3%"></th>
				<th data-field="delete" width="3%"></th>
			</tr>
		</thead>
		<tbody>
				
			<?php
        if (empty($traininglicences)) {
            ?>
					<tr>
				<th colspan="8" style="text-align: center;">There are no records.</th>
			</tr>
		<?php
        }
        
        foreach ($traininglicences as $traininglicence) {
            if ($traininglicence->getExpired() && ! $traininglicence->getNotifExpirySent()) {
                IntranetMail::TrainingLicenceExpiryEmail($traininglicence->getId());
                Intranet::getDbLink()->query('UPDATE `training_licences` SET `notif_w_sent`=1, `notif_e_sent`=1 WHERE `id`=\'' . $traininglicence->getId() . '\';');
            } else if ($traininglicence->getWarning() && ! $traininglicence->getNotifWarningSent()) {
                IntranetMail::TrainingLicenceWarningEmail($traininglicence->getId());
                Intranet::getDbLink()->query('UPDATE `training_licences` SET `notif_w_sent`=1 WHERE `id`=\'' . $traininglicence->getId() . '\';');
            }
            $user = User::fromDatabase($traininglicence->getRecipient());
            ?>
			<tr
				<?= $traininglicence->getExpired() ? ' style="color:#e53935"' : ($traininglicence->getWarning() ? ' style="color:#f57c00"' : '') ?>>
				<td><small><a href="./../View/?id=<?= $traininglicence->getId(); ?>"
						title="View this record."><b><?= $traininglicence->getName(); ?></b></a></small></td>
				<td><small><a class="tooltipped" data-position="top" data-delay="50"
						data-tooltip="<?= $user->getEmail(); ?>"
						href="<?= Config::getProperty('BaseDirWebserver'); ?>/Users/View/?id=<?= $user->getId(); ?>"><?= $user->getNameFull(); ?></a></small></td>
				<td><small><?= $traininglicence->getDate(); ?></small></td>
				<td><?= $traininglicence->getValidFor() == 0 ? '&infin;' : ('<small>' . $traininglicence->getValidUntil() . '</small>'); ?></td>
				<td style="text-align: center;"><a
					href="./?id=<?= $traininglicence->getId(); ?>&edit"
					title="Edit this record"><i class="material-icons tiny">mode_edit</i></a></td>
				<td style="text-align: center;"><a
					href="./../Delete/?id=<?= $traininglicence->getId(); ?>"
					title="Delete this record"><i class="material-icons tiny">delete</i></a></td>
			</tr>
			<?php
        }
        ?>
		</tbody>
	</table>
	<br>
	<div align="center">
		<a class="btn waves-effect waves-light cyan lighten-2" href="./../Add">Create
			Record</a> &nbsp; <a
			class="btn waves-effect waves-light light-blue darken-3"
			href="<?= (isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER']) && strpos($_SERVER['HTTP_REFERER'], 'delete') === FALSE) ? $_SERVER['HTTP_REFERER'] : Config::getProperty('BaseDirWebserver'); ?>">Back</a>
	</div>
	<br>
	</div>
	</div>
	</main>
	<?php
    }
    require_once (Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'style' . DIRECTORY_SEPARATOR . 'footer.php');
} else {
    header('Location: ' . Config::getProperty('BaseDirWebserver'));
}
