<?php
require_once (__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'config.php');
if (in_array($_SESSION['user']->getClass(), Intranet::$GROUP_CONTRIBUTORS)) {
    
    if (isset($_POST['submit'])) {
        $result = Intranet::getDbLink()->query('SELECT `id` FROM `users` WHERE CONCAT(`name_first`, \' \', `name_last`)=\'' . $_POST['recipient'] . '\';');
        $recipient = @$result->fetch_all()[0][0];
        
        if (is_null($recipient)) {
            header('Location: ./../Manage');
            die();
        }
        
        $id = GUID();
        $name = Intranet::getDbLink()->real_escape_string($_POST['name']);
        $description = Intranet::getDbLink()->real_escape_string($_POST['description']);
        $date = Intranet::getDbLink()->real_escape_string(date('Y-m-d', strtotime($_POST['date'])));
        $validfor = (int) Intranet::getDbLink()->real_escape_string($_POST['validfor']);
        $file_mimetype = empty($_FILES['file']['type']) ? '' : Intranet::getDbLink()->real_escape_string($_FILES['file']['type']);
        
        if ($_FILES['file']['name'] != '') {
            $filepath = Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'Training_Licences' . DIRECTORY_SEPARATOR . 'Files' . DIRECTORY_SEPARATOR . $id;
            if (file_exists($filepath)) {
                echo ('File already exists!');
            } else {
                move_uploaded_file($_FILES['file']['tmp_name'], $filepath);
            }
        }
        Intranet::getDbLink()->query('INSERT INTO `training_licences` (`id`, `recipient`, `name`, `description`, `date`, `validfor`, `file_mimetype`) VALUES (\'' . $id . '\', \'' . $recipient . '\', \'' . $name . '\', \'' . $description . '\', \'' . $date . '\', \'' . $validfor . '\', \'' . $file_mimetype . '\')');
        header('Location: ./../Manage');
    }
    
    require_once (Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'style' . DIRECTORY_SEPARATOR . 'header.php');
    ?>
<body class="light-blue darken-4">
	<main class="light-blue darken-3"> <br>
	<div class="container">
		<div class="card-panel">
			<h4 align="center">Add Training/Licence</h4>
			<form action="./" method="POST" enctype="multipart/form-data">
				<div class="input-field">
					<input id="recipient" name="recipient" type="text"
						class="autocomplete-recipient validate"> <label class="active"
						for="recipient">Recipient</label>
				</div>
				<div class="input-field">
					<input id="name" name="name" type="text"
						class="autocomplete-name validate"> <label class="active"
						for="name">Training/Licence Name</label>
				</div>
				<div class="input-field">
					<textarea id="description" name="description"
						class="materialize-textarea validate"></textarea>
					<label for="description">Training/Licence Description</label>
				</div>
				<div class="input-field">
					<input id="date" name="date" type="date" class="datepicker"> <label
						class="active" for="date">Date Effective (Click to Choose)</label>
				</div>
				<div class="file-field input-field">
					<div class="btn light-blue darken-3">
						<span>File (Optional)</span> <input type="file" id="file"
							name="file">
					</div>
					<div class="file-path-wrapper">
						<input class="file-path validate" type="text">
					</div>
				</div>
				<br>
				<div class="input-field">
					<select class="browser-default" id="validfor" name="validfor">
						<option value="0" disabled selected>Valid For (Years)</option>
						<option value="0">Infinite</option>
						<option value="1">1 Year</option>
						<option value="2">2 Years</option>
						<option value="3">3 Years</option>
						<option value="4">4 Years</option>
						<option value="5">5 Years</option>
					</select>
				</div>
				<br> <br>
				<button class="btn waves-effect waves-light cyan lighten-2"
					type="submit" name="submit">Submit</button>
				<br>
			</form>
		</div>
	</div>
	</main>
	<script type="text/javascript">
	$(document).ready(function () {
		$('input.autocomplete-recipient').autocomplete({
			data: {
<?php
    $result = Intranet::getDbLink()->query('SELECT `id` from `users` ORDER BY `name_last` ASC;');
    $users = $result->fetch_all();
    for ($i = 0; $i < count($users); $i ++) {
        $user = User::fromDatabase($users[$i][0]);
        echo ('"' . $user->getNameFull() . '" : null' . ($i == count($users) ? '' : ',') . PHP_EOL);
    }
    ?>
			}
		});

		$('input.autocomplete-name').autocomplete({
			data: {
<?php
    $result = Intranet::getDbLink()->query('SELECT DISTINCT `name` FROM `training_licences` ORDER BY `name` ASC;');
    $names = $result->fetch_all();
    for ($i = 0; $i < count($names); $i ++) {
        echo ('"' . $names[$i][0] . '" : null' . ($i == count($names) ? '' : ',') . PHP_EOL);
    }
    ?>
			}
		});
	});
	</script>
<?php
    require_once (Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'style' . DIRECTORY_SEPARATOR . 'footer.php');
}
