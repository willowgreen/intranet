<?php
require_once (__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'config.php');
$id = $_GET['id'];

$traininglicence = TrainingLicence::fromDatabase($id);

if (isset($_GET['file'])) {
    $file = Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'Training_Licences' . DIRECTORY_SEPARATOR . 'Files' . DIRECTORY_SEPARATOR . $traininglicence->getId();
    
    header('Content-description: File Transfer');
    header('Content-type: ' . $traininglicence->getFileMimeType());
    header('Content-disposition: inline; filename="' . $traininglicence->getFileName() . '"'); // 'inline' refers to showing the attachment in-browser if possible
    header('Expires: 0');
    header('Cache-control: must-revalidate');
    header('Pragma: public');
    header('Content-length: ' . filesize($file));
    readfile($file); // output the file's contents to the client
} else {
    $user = User::fromDatabase($traininglicence->getRecipient());
    require_once (Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'style' . DIRECTORY_SEPARATOR . 'header.php');
    ?>
<body class="light-blue darken-4">
	<main class="light-blue darken-3"> <br>
	<div class="container">
		<div class="card-panel">
			<h4><?= $traininglicence->getName(); ?></h4>
			<h6>Description: <?= $traininglicence->getDescription()=="" ? 'Record has no description.' : $traininglicence->getDescription(); ?></h6>
			<?php
    if ($traininglicence->getExpired()) {
        echo ('<h3 align="center" style="color:#e53935;">EXPIRED</h3>');
        echo ('<br>');
    }
    ?>
			<table>
				<tbody>
					<tr>
						<th>Recipient <small>(Click to Inspect)</small></th>
						<td><a
							href="<?= Config::getProperty('BaseDirWebserver'); ?>/Users/View/?id=<?= $user->getId(); ?>"><?= $user->getNameFull(); ?></a></td>
					</tr>
					<tr>
						<th>File <small>(Click to View/Download)</small></th>
						<td><?= $traininglicence->getFileExists() ? ('<a href="'. Config::getProperty('BaseDirWebserver').'/Training_Licences/View/?file&id=' . $traininglicence->getId() . '">' . $traininglicence->getFileName() . '</a>') : 'N/A';  ?></td>
					</tr>
					<tr>
						<th>Valid date</th>
						<td
							<?= $traininglicence->getValidFor() == 0 ? '' : ($traininglicence->getExpired() ? ' style="color:#e53935;"' : ($traininglicence->getWarning() ? ' style="color:#f57c00;"' : '')); ?>><?= $traininglicence->getDate() . ' &mdash; ' . ($traininglicence->getValidFor() == 0 ? '<big>&infin;</big>' : $traininglicence->getValidUntil()); ?></td>
					</tr>
					<tr>
						<th>ID</th>
						<td><code><?= $traininglicence->getId(); ?></code></td>
					</tr>
				</tbody>
			</table>
			<br>
			<p align="center">
		<?php
    if ($traininglicence->getFileExists()) {
        ?>
		<a class="waves-effect waves-light btn light-blue darken-3"
					href="./?id=<?= $traininglicence->getId(); ?>&file">View file</a> &nbsp;
	<?php
    }
    ?>
				<a class="waves-effect waves-light btn light-blue darken-3"
					href="./../Manage/?id=<?= $traininglicence->getId(); ?>&edit">Edit</a>
				&nbsp; <a class="waves-effect waves-light btn red darken-3"
					href="./../Delete?id=<?= $traininglicence->getId(); ?>">Delete</a>
			</p>
			<p align="center">
				<a class="waves-effect waves-light btn light-blue darken-3"
					href="<?= (isset($_SERVER['HTTP_REFERER']) && !empty($_SERVER['HTTP_REFERER'])) ? $_SERVER['HTTP_REFERER'] : './../Manage'; ?>"
					title="Go back">Back</a>
			</p>
		</div>
	</div>
	</main>
<?php
    require_once (Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'style' . DIRECTORY_SEPARATOR . 'footer.php');
}
