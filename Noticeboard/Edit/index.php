<?php
require_once (__DIR__ . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'config.php');
if (in_array($_SESSION['user']->getClass(), Intranet::$GROUP_CONTRIBUTORS)) {
    if (isset($_POST['submit'])) {
        $result = Intranet::getDbLink()->query('UPDATE `noticeboard` SET `message`=\'' . Intranet::getDbLink()->real_escape_string($_POST['noticeboard']) . '\', `updater`=\'' . $_SESSION['user']->getId() . '\' WHERE 1;');
        header('Location: ' . Config::getProperty('BaseDirWebserver'));
    }
    
    require_once (Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'style' . DIRECTORY_SEPARATOR . 'header.php');
    ?>
<body class="light-blue darken-4">
	<main class="light-blue darken-3"> <br>
	<div class="container">
		<div class="card-panel">
			<h4 align="center">Edit Noticeboard</h4>
	<?php
    $result = Intranet::getDbLink()->query('SELECT * FROM `noticeboard`;');
    $notices = $result->fetch_assoc();
    
    $updater = User::fromDatabase($notices['updater']);
    ?>
			<form action="./" method="POST">
				<div align="center">
					<div class="input-field">
						<textarea id="noticeboard" name="noticeboard"
							class="materialize-textarea"><?= $notices['message'] ?></textarea>
						<label for="noticeboard">Edit Noticeboard</label>
						<p>Last updated <?= date('j F Y, g:i a', strtotime($notices['updated'])); ?> by <a
								href="<?= Config::getProperty('BaseDirWebserver'); ?>/Users/View/?id=<?= $updater->getId(); ?>"><?= $updater->getNameFull(); ?></a>
						</p>
					</div>
					<br>
					<div align="center">
						<button class="btn waves-effect waves-light cyan lighten-2"
							type="submit" name="submit">Submit</button>
						&nbsp; <a class="btn waves-effect waves-light light-blue darken-3"
							href="<?= isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : Config::getProperty('BaseDirWebserver') . '/Documents/Manage'; ?>">Back</a>
					</div>
					<br>
				</div>
			</form>
		</div>
	</div>
	</main>
<?php
    require_once (Config::getProperty('BaseDir') . DIRECTORY_SEPARATOR . 'style' . DIRECTORY_SEPARATOR . 'footer.php');
} else
    header('Location: ' . Config::getProperty('BaseDirWebserver'));
