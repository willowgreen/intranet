<html>
<head>
<title>Create password hash</title>
</head>
<body>
	<h1>Create password hash</h1>
	<p>using default BCRYPT algorithm</p>
<?php
if (isset($_POST['submit'])) {
    if ($_POST['password1'] == $_POST['password2']) {
        ?>
		<input type="text" name="passwordhash" id="passwordhash"
		value="<?= password_hash($_POST['password1'], PASSWORD_BCRYPT) ?>"
		style="width: 85%;">

	<script type="text/javascript">
		(function() {
			document.getElementById("passwordhash").select();
		})();
		</script>
	<br>
	<br>
		<?php
    }
}
?>
	<form action="./" method="POST">
		Password: <br> <input type="password" name="password1"> <br> <br>
		Confirm:<br> <input type="password" name="password2"> <br> <br> <input
			type="submit" name="submit" value="Submit">
	</form>
</body>
</html>